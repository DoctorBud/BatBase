@db-val
Feature: Custom form-validation alerts editor when there are validation issues.
    In order to enter accurate data into the bat eco-interactions database
    As an editor
    I need to see UI alerts when there are validation issues in the form data.

    ### WHAT IS BEING TESTED ###

    Background:
        Given the fixtures have been reloaded
        And I am on "/login"
        And I fill in "_username" with "test.editor@email.com"
        And I fill in "_password" with "passwordhere"
        And I press the "_submit" button
        And I am on "/search"
        And I see "TestEditor"
        And the database has loaded
        And I exit the tutorial
  ## --------------------- INTERACTION VALIDATION --------------------------- ##
    @javascript
    Scenario:  I should see an alert if there are no valid interaction types for
        the selected taxon groups

        Given I press the "New" button
        And I break "Open console"
        And I see "New Interaction"
        ## --- SUBJECT --- ##
        When I focus on the "Subject" taxon field
        And I see "Select Subject Taxon"
        And I select "Reptile" from the "Group" combobox
        And I press the "Select Unspecified" button
        And I wait for the "sub" form to close
        ## --- OBJECT --- ##
        When I focus on the "Object" taxon field
        And I see "Select Object Taxon"
        And I select "Bird" from the "Group" combobox
        And I press the "Select Unspecified" button
        And I submit the form
        And I wait for the "sub" form to close
        ## --- ALERT --- ##
        Then I should see a form "Interaction Type" alert
        And I focus on the "Subject" taxon field
        And I see "Select Subject Taxon"
        And I select "Bat" from the "Group" combobox
        And I press the "Select Unspecified" button
        And the alert for the "Interaction Type" field should clear

  ## ----------------------- SOURCE VALIDATION ------------------------------ ##
   # Note: Validation happens on form-submit
    @javascript
    Scenario:  I should see an alert if I try to create an author|editor with the same
        display name as another author|editor.

        Given I press the "New" button
        And I see "New Interaction"
        And I add "Test Book" to the "Publication" combobox
        And I see "New Publication"
        And I select "Book" from the "Publication Type" combobox
        When I add "New Author" to the "Author" dynamic combobox
        ## --- ALERT --- ##
        And I type "Anya" in the "First Name" field "input"
        And I type "Cockle" in the "Last Name" field "input"
        And I submit the form
        Then I should see a form "sub2" alert

    @javascript
    Scenario:  I should see an alert if there is a blank in the selected author order
        Given I press the "New" button
        And I see "New Interaction"
        And I add "Test Book" to the "Publication" combobox
        And I see "New Publication"
        And I select "Book" from the "Publication Type" combobox
        And I select "Bloedel, P" from the "Editor" dynamic combobox
        And I select "Cockle, Anya" from the "Editor" dynamic combobox
        And I select "Baker, Herbert G" from the "Editor" dynamic combobox
        ## --- ALERT --- ##
        When I clear the "2nd" "Editor" dynamic combobox
        Then I should see a form "Editor" alert

    #Note: Validation happens on form-submit
    @javascript
    Scenario:  I should see an alert if I enter an invalid page-range
        Given I press the "New" button
        And I see "New Interaction"
        And I select "Journal of Mammalogy" from the "Publication" combobox
        And I add "Test Citation" to the "Citation Title" combobox
        And I select "Baker, Herbert G" from the "Author" dynamic combobox
        And I type "2000" in the "Year" field "input"
        ## --- ALERT --- ##
        When I type "999-666" in the "Pages" field "input"
        And I submit the form
        Then I should see a form "Pages" alert
        And I type "666-999" in the "Pages" field "input"
        And the alert for the "Pages" field should clear

  ## ---------------------- LOCATION VALIDATION ----------------------------- ##
    @javascript
    Scenario:  I should see an alert if I create an unspecific location that already exists
        Given I press the "New" button
        And I see "New Interaction"
        And I add "New" to the "Location" combobox
        And I see "New Location"
        ## --- ALERT --- ##
        When I type "Name" in the "Display Name" field "input"
        And I select "Costa Rica" from the "Country" combobox
        And I select "Savanna" from the "Habitat Type" combobox
        And I submit the form
        Then I should see a form "sub" alert
        And I click on the "sub" alert
        And I wait for the "sub" form to close
        And I should see "Costa Rica-Savanna" in the "Location" combobox

    @javascript
    Scenario:  I should see an alert if I enter an invalid elevation-range
        Given I press the "New" button
        And I see "New Interaction"
        And I add "New" to the "Location" combobox
        And I see "New Location"
        And I select "Costa Rica" from the "Country" combobox
        ## --- ALERT --- ##
        When I type "600" in the "Elevation" field "input"
        And I type "300" in the "Elevation Max" field "input"
        And I submit the form
        Then I should see a form "Elevation Max" alert
        When I type "900" in the "Elevation Max" field "input"
        And the alert for the "Elevation Max" field should clear

  ## ------------------------ TAXON VALIDATION ------------------------------ ##
    @javascript
    Scenario:  I should see an alert if I try to create a genus or species taxon
        without a Family selected AND if I try to create a species taxon without a
        Genus selected AND if I try to save a species with an incorrect binomial

        Given I press the "New" button
        And I see "New Interaction"
        And I focus on the "Subject" taxon field
        And I see "Select Subject Taxon"
        ## --- ALERT --- ##
        When I add "test" to the "Genus" combobox
        Then I should see a form "Genus" alert
        And I select "Phyllostomidae" from the "Family" combobox
        And the alert for the "Genus" field should clear
        ## --- ALERT --- ##
        When I add "test" to the "Species" combobox
        Then I should see a form "Species" alert
        And I select "Artibeus" from the "Genus" combobox
        And the alert for the "Species" field should clear
        ## --- ALERT --- ##
        When I add "Test Species" to the "Species" combobox
        And I see "New Taxon Species"
        And I submit the form
        And I should see a form "Species" alert
        And I type "Artibeus Species" in the "Display Name" field "input"
        And I submit the form
        And I wait for the "sub2" form to close

    @javascript
    Scenario:  I should see an alert if I try to change the level of a genus taxon
        with species children

        Given the database table is grouped by "Taxa"
        And I view interactions by "Bats"
        And I expand "Family Phyllostomidae" in the data tree
        And I click on the edit pencil for the "Genus Artibeus" row
        And I see "Editing Taxon"
        ## --- ALERT --- ##
        When I select "Family" from the "Rank" combobox
        Then I should see a form "Rank" alert
        And I select "Genus" from the "Rank" combobox
        And the alert for the "Rank" field should clear

    @javascript
    Scenario:  I should see an alert if I try to set a non-genus parent for a species taxon
        Given the database table is grouped by "Taxa"
        And I view interactions by "Bats"
        And I expand "Family Phyllostomidae" in the data tree
        And I expand "Genus Artibeus" in the data tree
        And I click on the edit pencil for the "Artibeus lituratus" row
        And I see "Editing Taxon"
        And I focus on the "Parent" taxon field
        ## --- ALERT --- ##
        And I press the "Select Unspecified" button
        Then I should see a form "Parent" alert
        And I press the "Select Taxon" button
        And I wait for the "sub2" form to close

    @javascript
    Scenario:  I should see an alert if I try to select a rank that is lower than
        that of the taxon's children AND if I select a taxon-parent that is a
        lower rank than the taxon being edited.

        Given the database table is grouped by "Taxa"
        And I view interactions by "Bats"
        And I click on the edit pencil for the "Family Phyllostomidae" row
        And I see "Editing Taxon"
        ## --- ALERT --- ##
        When I select "Genus" from the "Rank" combobox
        Then I should see a form "Rank" alert
        And I select "Family" from the "Rank" combobox
        And the alert for the "Rank" field should clear
        ## --- ALERT --- ##
        And I focus on the "Parent" taxon field
        And I see "Select Parent Taxon"
        When I select "Rhinophylla" from the "Genus" combobox
        And I press the "Select Taxon" button
        Then I should see a form "Parent" alert