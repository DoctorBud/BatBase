const Encore = require('@symfony/webpack-encore');
/* ========= PROD ======= */
// eslint-disable-next-line no-unused-vars
const SentryWebpackPlugin = require('@sentry/webpack-plugin');
/* ======== ALL =========== */
const autoProvidedVars = { L: 'leaflet', $: 'jquery', jQuery: 'jquery', Sentry: '@sentry/browser' };
const path = require('path');
// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}
/** ======================== Configuration ================================== */
Encore
/* ======== DEV ======= */
    /* public path used by the web server to access the output path */
    .setPublicPath('/BatBase/public_html/build')
/* ========= SERVER ======= */
    /* public path used by the web server to access the output path */
    // .setPublicPath('/build')
    /* PUSH TO MASTER FIRST. Sends source maps to Sentry for bug/issue tracking. */
    // .addPlugin(new SentryWebpackPlugin({
    //     // include: '.', test: [/\.js$/], release: 'test_feature',
    //     include: '.', test: [/\.js$/], release: '20220604_BB',
    //     debug: true, ignore: ['web', 'node_modules', 'webpack.config.js',
    //         'vendor', '/assets/js/libs/*', '/assets/libs/*', 'var', 'features'],
    // }))
/* ======== ALL =========== */
    // directory where compiled assets will be stored
    .setOutputPath('public_html/build')
    // only needed for CDN's or sub-directory deploy
    // .setManifestKeyPrefix('build')
    // enable source maps
    .enableSourceMaps(true)
    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()
    // show OS notifications when builds finish/fail
    .enableBuildNotifications(true, (options) => {
        options.alwaysNotify = true;
    })
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    // you can use this method to provide other common global variables,
    // such as '_' for the 'underscore' library
    .autoProvideVariables(autoProvidedVars)
    /** ------- Loaders ----------------- */
    .enableStylusLoader()
    // .configureLoaderRule('images', loaderRule => {
    //     loaderRule.test = /\.(png|svg|jpe?g|gif)$/;
    //     loaderRule.options = { name: 'images/[name].[hash:8].[ext]' };
    // })
    .addLoader({
        test: /\.(pdf)$/,
        use: [ {
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: './assets/files/'
            }
        }]
    })
    /** ------- Files to process ----------------- */
    .copyFiles([{
        from: './assets/images',
        to: 'images/[name].[ext]'
    },{
        from: './assets/files',
        to: 'files/[name].[ext]'
    }])
    /** ------- Site Js/Style Entries ----------------- */
    .addEntry('app', './assets/js/main.js')
    .addEntry('db', './assets/js/page/database/db-main.js')
    .addEntry('feedback', './assets/js/page/feedback/feedback-viewer.js')
    .addEntry('pdfs', './assets/js/page/view-pdfs.js')
    .addEntry('entity', './assets/js/page/entity/entity-show.js')
    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()
    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks();

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())
/** ======================== FINALIZE ======================================= */
const config = Encore.getWebpackConfig();

config.resolve.alias["db"] = path.resolve(__dirname, 'assets/js/page/database/');
config.resolve.alias["~db"] = path.resolve(__dirname, 'assets/js/page/database/db-main.js');
config.resolve.alias["~form"] = path.resolve(__dirname, 'assets/js/page/database/data-entry/data-entry-main.js');
config.resolve.alias["images"] = path.resolve(__dirname, 'assets/images/');
config.resolve.alias["styles"] = path.resolve(__dirname, 'assets/styles/');
config.resolve.alias["libs"] = path.resolve(__dirname, 'assets/libs/');
config.resolve.alias["~util"] = path.resolve(__dirname, 'assets/js/util/util-main.js');

config.optimization.emitOnErrors = true;
/* Force Webpack to display errors/warnings */
// config.stats.errors = true;
// config.stats.warnings = true;

module.exports = config;