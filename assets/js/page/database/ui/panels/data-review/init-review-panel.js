/**
 * Handles opening and closing the Data-Review Panel.
 *
 * Export
 *     initReviewPanel
 *
 * TOC
 *     INIT
 *     TOGGLE
 */
import { _table } from '~db';
import * as pM from '../panels-main.js';

/* ============================ INIT ======================================== */
export default function initReviewPanelUi(userRole) {
    if (userRole === 'visitor' || userRole === 'user') { return; }
    require('styles/pages/db/panels/rvw-data.styl');
    $('#rvw-data').click(toggleReviewPanel);
}
/* ========================== TOGGLE ======================================== */
function toggleReviewPanel() {
    if ($('#review-pnl').hasClass('closed')) {
        buildAndShowReviewPanel();
    } else { hideReviewPanel(); }
}
function toggleDataOptsBarButtons(enable = true) {
    const opac = enable ? 1 : .3;
    const cursor = enable ? 'pointer' : 'not-allowed';
    $('#new-data, #data-help').attr('disabled', !enable)
        .css({ 'opacity': opac, 'cursor': cursor });
}
/* ------------------------------- SHOW ------------------------------------- */
function buildAndShowReviewPanel() {
    pM.togglePanel('review', 'open');
    $('#data-opts').append(getPseudoBorderStyle());
    toggleDataOptsBarButtons(false);
    showUpdatedByColumn();
}
function getPseudoBorderStyle() {
    const panelT = $('#review-pnl').position().top;
    const tabW = $('#data-opts').innerWidth();
    const tabL = $('#data-opts').position().left + 1;                /*dbug-log*///console.log('sizePanelTab. T = [%s], W = [%s], L = [%s]', panelT, tabW, tabL);
    return `<style>.hide-rvw-bttm-border:before {
        position: absolute;
        content: '';
        height: 3px;
        z-index: 10;
        width: ${tabW}px;
        top: ${panelT}px;
        left: ${tabL}px;
        background: #d6ebff;
        }</style>`;
}
function showUpdatedByColumn(show = true) {
    const tblState = _table('tableState').get();
    tblState.columnApi.setColumnsVisible(['updatedBy'], show);
}
/* ------------------------------- HIDE ------------------------------------- */
function hideReviewPanel() {
    pM.togglePanel('review', 'close');
    $('.hide-rvw-bttm-border:before').remove();
    toggleDataOptsBarButtons(true);
    showUpdatedByColumn(false);
}