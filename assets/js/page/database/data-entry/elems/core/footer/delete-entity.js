/**
 * Temporary module to allow admin to handle deleting interactions.
 *
 * Export
 *     ifAdminGetDeleteButton
 *
 * TOC
 *     CONFIRM MODAL
 *     DELETE ENTITY
 */
import { _db, _modal, _u } from '~util';
import { _elems, _state, _val } from '~form';

export default function ifAdminGetDeleteButton(entity, action, bttn) {
    if (!isAdminEditingInteraction(entity, action, _u('isAdminUser'))) { return; }
    $(bttn).click(confirmDelete);
    return bttn;
}
function isAdminEditingInteraction(entity, action, isAdmin) {
    return isAdmin && action === 'edit' && entity === 'Interaction';
}
/* ======================= CONFIRM MODAL ==================================== */
function confirmDelete() {                                          /*dbug-log*///console.log('    -- confirmDelete');
    _modal('showSaveModal', [ buildModalConfg() ]);
    $(`#top-submit`).css({opacity: .5, cursor: 'not-allowed'})
    window.setTimeout(() => $('.modal-msg').css({width: 'max-content'}), 500);
}
function buildModalConfg() {
    return {
        html: getDeleteHtml(),
        selector: `#top-delete`,
        dir: 'right',
        submit: deleteEntity,
        bttn: 'CONFIRM  DELETE'
    };
}
function getDeleteHtml() {
    return 'Are you sure this interaction is ready to be deleted?';
}
/* ======================= DELETE ENTITY ==================================== */
function deleteEntity() {
    const data = {
        coreEntity: 'Interaction',
        id: _state('getFormState', ['top', 'editing']).core
    };                                                              /*dbug-log*///console.log('    -- deleteEntity data[%O]', data);
    _state('setFormState', ['top', 'submitted', true]);
    _state('setFormState', ['top', 'action', 'delete']);
    pushData(data);
}
function pushData(data) {
    const url = `crud/entity/delete`;
    _u('sendAjaxQuery', [data, url, pushSuccess, pushError]);                   _u('logInProdEnv', ['data = ', JSON.stringify(data)]);

    function pushError() {
        _elems('toggleWaitOverlay', [false]);
        _val('formSubmitError', ['top', ...arguments]);
    }
    function pushSuccess(data) {                                                _u('logAjaxData', [data, arguments]);
        if (data.results.error) { return pushError(null, data.results.error); }
        _db('afterDataEntrySyncLocalDatabase', [data.results, null])
        .then(() => _elems('toggleWaitOverlay', [false]))
        .then(() => _elems('exitRootForm'));
    }
}