/**
 * The form side-panel is used for the bulk of the UI
 *
 * EXPORT
 *`getDataReviewElems`
 *
 * TOC
 *     CONTRIBUTOR ELEMS
 *     EDITOR ELEMS
 *     EXPERT ELEMS
 *     MANAGER ELEMS
 */
import { _state } from '~form';
import { _el } from '~util';
/* ____________________ BUILD DATA-REVIEW ELEMS _____________________________ */
export function getDataReviewElems(fConfg) {                        /*dbug-log*///console.log("-- getDataReviewElems fConfg[%O]", fConfg);
    const infoElems = [ getDataReviewIntro(), callReviewMethod('intro') ];
    const fReviewElems = getFormReviewElems(fConfg);
    return [ ...infoElems, fReviewElems ];
}
/* ---------------------------- INTRO --------------------------------------- */
function getDataReviewIntro() {
    const txt = getDataReviewIntroTxt();
    const attr = { text: txt };
    return _el('getElem', ['p', attr]);
}
function getDataReviewIntroTxt() {
    return `Data are reviewed and either approved, rejected, or returned for
        further action. They are only available to their contributor until approved.`;
}
/* ------------------------- SUB-FORM --------------------------------------- */
export function initEntityDataReviewElems(fConfg) {
    if (fConfg.action === 'select') { return; }
    initSubFormDataReviewElems(fConfg);
    toggleParentFormReviewSection(fConfg.group);
}
function initSubFormDataReviewElems(fConfg) {
    const cntnr = getFormReviewElems(fConfg);
    $('#ent-id').before(cntnr);
    indentSubFormContainer(cntnr, fConfg.group);
    _el('setElemExpandableListeners', [$(`${fConfg.group}-rvw`)])
}
function toggleParentFormReviewSection(fLvl) {
    const prntLvl = _state('getFormLevel',  ['parent', fLvl]);
    _el('toggleContent', [`.${prntLvl}-rvw`]);
}
function removeSubFormReviewSection(fLvl) {
    const childLvl = _state('getFormLevel',  ['child', fLvl]);
    $(`#${childLvl}-rvw`).fadeTo(500, 0, () => $(`#${childLvl}-rvw`).remove());
}
export function onSubFormCloseUpdateReviewPanel(fLvl) {
    if (!$(`#${fLvl}-rvw`)[0]) { return; } //select-forms don't have data-review
    $(`#${fLvl}-rvw`).fadeTo(500, 0, () => updateReviewPanel(fLvl));
    removeSubFormReviewSection(fLvl);
}
function updateReviewPanel(fLvl) {
    $(`#${fLvl}-rvw`).remove();
    toggleParentFormReviewSection(fLvl);
}
/* ======================= CORE STRUCTURE =================================== */
function getFormReviewElems(fConfg) {
    const cntnr = buildEntityReviewContainer(fConfg.group);
    const fElems = [
        getReviewHeader(fConfg.group, fConfg.name),
        buildReviewContent()
    ];
    $(cntnr).append(fElems);
    return cntnr;
}
function buildEntityReviewContainer(fLvl) {
    const attr = { class: 'expandable', id: fLvl+'-rvw' };
    return  _el('getElem', ['div', attr]);
}
/* ------------------------------ HEADER ------------------------------------ */
//todo: add tutorial button
function getReviewHeader(fLvl, entity) {  //todo: add tutorial button
    const attr = { class: `toggle-trigger ${fLvl}-rvw`, text: `${entity} Data-Review` };
    const hdr = _el('getElem', ['h4', attr]);
    $(hdr).data('expanded', false).data('collapsed', false);
    return hdr;
}
/* ------------------------------ CONTENT ----------------------------------- */
function buildReviewContent() {
    const cntnr = getContentContainer();
    $(cntnr).append(getUserDataReviewContent());
    return cntnr;
}
function getContentContainer() {
    const attr = { class: 'expandable-content' };
    return _el('getElem', ['div', attr]);
}
function getUserDataReviewContent() {
    const elems = [
        // callReviewMethod('manager'),
        buildDataReviewNotesElems(),
        // callReviewMethod('actions')
    ];
    return elems;
}
/* ------------------------------ STYLE ------------------------------------- */
function indentSubFormContainer(cntnr, fLvl) {
    const indent = { sub: 1, sub2: 2 };
    $(cntnr).css({'margin-left': `${indent[fLvl]}em`});
}

/* ==================== USER-ROLE BUILDERS  ========================================================================= */
function callReviewMethod(stage, params = []) {
    // const role = _u('getUserRole');
    const role = 'contributor';
    const map = {
        contributor: {
            intro: getContributorIntro,
            // actions: getContributorOutro,
        },
        // editor: getEditorReviewElems,
        // expert: getExpertReviewElems,
        // manager: getManagerReviewElems
    };
    return map[role][stage] ? map[role][stage](...params) : Function.prototype;
}


/**
 * Rely on walkthrough and tooltips to fully explain
 *
 * Data-Review
 *     Contributer: Data entered will be reviewed before approval.
 *
 * Messages
 *     (placeholder: Add comments, questions, and/or additional relevant-data. )
 *
 *
 */

/* ==================== CONTRIBUTOR ELEMS =================================== */
/* ---------------------------- INTRO --------------------------------------- */
function getContributorIntro() {
    const attr = { text: getContributorIntroTxt() };
    return _el('getElem', ['p', attr]);
}
function getContributorIntroTxt() {
    return `Once reviewed, results will load in your database "Review" tab.`;
}
/* ==================== EDITOR ELEMS ======================================== */
/* ==================== EXPERT ELEMS ======================================== */
/* ==================== MANAGER ELEMS ======================================= */
/**
 * Rely on walkthrough and tooltips to fully explain
 *
 * Data-Review
 *
 * Messages
 *     (placeholder: Add comments, questions, and/or additional relevant-data. )
 *
 * Actions
 *
 *
 */
function getManagerReviewElems() {
    //modify existing form-elems to allow notes to be added to rejected/changed data
    return [];
}
/* ====================== SHARED BUILDERS =========================================================================== */

/* ====================== FEATURE MESSAGES ================================== */
function buildDataReviewNotesElems() {
    const cntnr = getMessageContainer();
    const elems = [
        getMessagesHeader(),
        getMessageHistory(),
        getNewMessageInput(),
    //  submit buttons
    ];
    $(cntnr).append(elems);
    return cntnr;
}
function getMessageContainer() {
    const attr = { id: 'rvw-msg-cntnr' };
    return _el('getElem', ['div', attr]);
}
function getMessagesHeader() {
    const attr = { text:  `Notes` }
    return _el('getElem', ['h4', attr]);
}
function getMessageHistory() {
    const history = 'No notes yet.'
    const attr = { class: 'rvw-msgs', id:  `rvw-msg-hstry`, disabled: true, text: history }
    return _el('getElem', ['textarea', attr]);
}
function getNewMessageInput() {
    const attr = { class: 'rvw-msgs', id:  `rvw-msg`, placeholder: 'Add a note for the data-manager...' }
    return _el('getElem', ['textarea', attr]);
}
/* ---------------------------- INTRO --------------------------------------- */
/* ---------------------------- OUTRO --------------------------------------- */


/* ====================== FEATURE OUTRO ===================================== */
