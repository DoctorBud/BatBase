/**
 * Returns side-panel assembled with passed form-entity and data-review elems.
 */
import { _el, _u } from '~util';
/* ===================== INIT SIDE-PANEL ================================== */
export default function buildSidePanel(fConfg, fElems, rElems) {    /*dbug-log*///console.log("--buildSidePanel fConfg[%O] fElems[%O], rElems[%O]", fConfg, fElems, rElems);
    const cntnr = buildPanelContainer(fConfg.name);
    $(cntnr).append(buildPanelHeader(fConfg.name));
    $(cntnr).append([...fElems, '<hr>', ...rElems]);
    $(cntnr).append(getEntityIdFooter(fConfg.editing));
    return cntnr;
}
function buildPanelContainer(entity) {
    const attr = { id: 'form-panel', class: _u('lcfirst', [entity]) };
    return _el('getElem', ['div', attr]);
}
function buildPanelHeader(entity) {
    const attr = { text:  `${entity} Record` }
    return _el('getElem', ['h3', attr]);
}
function getEntityIdFooter(eIds) {
    // if (!eIds) { return; }
    const txt = eIds ? `Id: ${eIds.core}` : '';
    const attr = { id: 'ent-id', text: txt }
    return _el('getElem', ['p', attr]);
}