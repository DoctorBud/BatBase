/**
 * Initialize data-entry form.
 *
 * Export
 *     alertInUse
 *     finishFormBuild
 *     ifFormInUse
 *     initForm
 *     initSubForm
 *
 * TOC
 *     TOP FORM
 *     SUB FORM
 *         IF OPEN SUB-FORM ISSUE
 *         UPDATE TOP-FORM
 *     BUILD CHAIN
 *         BUILD FORM-ELEMS
 *         PANEL ELEMS
 *         FINISH BUILD
 *     ASSEMBLE FORM
 *         CONTAINER
 *         HEADER
 *         STATUS/ALERTS
 */
import { _cmbx, _el } from '~util';
import { _elems, _state, _val } from '~form';
let fState;
/* ======================== TOP FORM ======================================== */
export function initForm(p) {                                       /*dbug-log*///console.log('+--initForm params[%O]', p);
    p.group = 'top';
    return _state('initFormState', [p])
        .then(fS => initFormElems(fS.forms.top, p.appendForm))
        .then(fState => finishFormBuild(p.name, p.group, p.initCombos))
        .then(() => _el('setElemExpandableListeners', [$('#form-panel')]))
        .then(() => 'success');
}
/* ======================== SUB FORM ======================================== */
export function initSubForm(p) {                                    /*dbug-log*///console.log('+--initSubForm params[%O]', p);
    if (ifFormInUse(p.group)) { return alertInUse(p.combo, p.group); }
    return _state('buildNewFormState', [p])
        .then(fState => initFormElems(fState, p.appendForm))
        // .then(fState => _elems('initEntityDataReviewElems', [fState]))
        .then(() => finishFormBuild(p.name, p.group, p.initCombos))
        .then(() => updateParentForm(p))
        .then(() => 'success');
}
/* ----------------- IF OPEN SUB-FORM ISSUE --------------------------------- */
export function ifFormInUse(fLvl) {                                 /*dbug-log*///console.log('--ifFormInUse fLvl[%s]', fLvl);
    return fLvl ? $(`#${fLvl}-form`).length !== 0 : false;
}
export function alertInUse(name, fLvl) {                            /*dbug-log*///console.log('--alertInUse name[%s] fLvl[%O]', name, fLvl);
    _val('alertFormOpen', [fLvl]);
    _cmbx('resetCombobox', [name]);
    return Promise.resolve();
}
/* -------------------- UPDATE TOP-FORM ------------------------------------- */
function updateParentForm(p) {                                      /*dbug-log*///console.log('--updateParentForm');
    const pLvl = _state('getFormLevel', ['parent', p.group]);
    _elems('toggleSubmitBttn', [pLvl, false]);
    _cmbx('enableCombobox', [p.combo, false])
}
/* ======================== BUILD CHAIN ===================================== */
function initFormElems(fState, appendFunc) {                        /*dbug-log*///console.log('--initFormElems fState[%O] appendFunc[%O]', fState, appendFunc);
    return _elems('getFormPieces', [fState])
        .then(elems => buildAndAppendForm(elems, fState, appendFunc))
        .then(() => fState);
}
/* ----------------------- BUILD FORM-ELEMS --------------------------------- */
function buildAndAppendForm(elems, fState, appendFunc) {             /*dbug-log*///console.log('--assembleAndAppend elems[%O] fState[%O] appendFunc[%O]', elems, fState, appendFunc);
    const form = assembleFormElems(elems, fState);
    if (fState.group === 'top') {
        appendFunc(form, fState, elems)
    } else {
        appendFunc(form)
    }
}
/* -------------------- FINISH BUILD ---------------------------------------- */
export function finishFormBuild(entity, fLvl, initCombos) {         /*dbug-log*///console.log('--finishFormBuild initCombos?[%O]', initCombos);
    _elems('setDynamicFieldStyles', [entity]);
    if (initCombos) { return initCombos(); }
}
/* ======================== ASSEMBLE FORM =================================== */
function assembleFormElems(el, formState) {                         /*dbug-log*///console.log('+--assembleFormElems elems[%O] fState[%O]', el, _u('snapshot', [formState]));
    fState = formState;
    const cntnr = buildFormCntnr();
    const hdr = buildFormHdr(el.tutBttn);
    const valMsg = getValMsgCntnr();
    $(cntnr).append([hdr, valMsg, el.rows, el.footer].filter(e=>e));
    $(cntnr).submit(e => e.preventDefault());
    return cntnr;
}
/* --------------------------- CONTAINER ------------------------------------ */
function buildFormCntnr() {
    const attr = {id: fState.group+'-form', class: fState.style };
    return _el('getElem', ['form', attr]);
}
/* ------------------------------ HEADER ------------------------------------ */
function buildFormHdr(tutBttn) {
    const cntnr = buildHeaderCntnr();
    const hdr = _el('getElem', ['span', { text: getHeaderTitle() }]);
    $(cntnr).append([getBttnOrSpacer(tutBttn), hdr, $('<div>')[0]]);
    return cntnr;
}
function buildHeaderCntnr() {
    return _el('getElem', ['div', { id: fState.group+'-hdr', class: 'flex-row'}]);
}
function getHeaderTitle() {
    const map = {
        create: 'New',
        edit: 'Edit',
        select: 'Select'
    };
    return map[fState.action] + ' ' + fState.name;
}
function getBttnOrSpacer(tutBttn) {
    return fState.group === 'top' ? $('<div>')[0] : tutBttn;
}
/* ------------------------- STATUS/ALERTS ---------------------------------- */
/** Container for custom form-validation messages. */
function getValMsgCntnr() {
    return _el('getElem', ['div', { id: fState.group+'_alert' }]);
}