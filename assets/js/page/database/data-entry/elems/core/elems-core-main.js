/**
 * Initiates and appends the main entity form.
 *
 * TOC
 *     INIT BUILD
 *         IF OPEN SUB-FORM ISSUE
 *         BUILD FORM
 *         SIDE-PANEL
 *     FORM PIECES
 *     EXIT FORM
 */
import { _elems } from '~form';
import * as form from './form/form-build-main.js';
import * as bttn from './buttons/form-buttons.js';
import * as panel from './side-panel/side-panel-main.js';
import * as footer from './footer/form-footer-main.js';
/* ============================= BUILD FORM ================================= */
export function initForm() {
    return form.initForm(...arguments);
}
export function initSubForm() {
    return form.initSubForm(...arguments);
}
export function finishFormBuild() {
    form.finishFormBuild(...arguments);
}
/* ----------------- IF OPEN SUB-FORM ISSUE --------------------------------- */
export function ifFormInUse() {
    return form.ifFormInUse(...arguments);
}
export function alertInUse() {
    return form.alertInUse(...arguments);
}
/* ---------------------------- SIDE-PANEL -------------------------------- */
/** ----------- INIT -------------- */
export function getSidePanelElems() {
    return panel.getSidePanelElems(...arguments);
}
export function initEntityDataReviewElems() {
    return panel.initEntityDataReviewElems(...arguments);
}
/** ------------ SET -------------- */
export function setSubEntityDetails() {
    return panel.setSubEntityDetails(...arguments);
}
export function fillEditEntitySidePanel() {
    return panel.fillEditEntitySidePanel(...arguments);
}
/** ---------- CLEAR -------------- */
export function clearSidePanelDetails() {
    return panel.clearSidePanelDetails(...arguments);
}
export function onSubFormCloseUpdateReviewPanel() {
    panel.onSubFormCloseUpdateReviewPanel(...arguments);
}
/* ============================== EXIT FORM ================================= */
export function getExitButton() {
    return bttn.getExitButton(...arguments);
}
/**
 * Removes the form container with the passed id, clears and enables the combobox,
 * and contextually enables to parent form's submit button. Calls the exit
 * handler stored in the form's params object.
 */
export function exitSubForm() {
    form.exitSubForm(...arguments);
}
/** Returns popup and overlay to their original/default state. */
export function exitRootForm() {
    form.exitRootForm(...arguments);
}
/* ========================= FORM PIECES ==================================== */
export function getFormPieces(fState) {
    const elems = {
        footer: footer.getFormFooter(fState.name, fState.group, fState.action),
        tutBttn: bttn.getFormHelpElems(fState.group, fState.infoSteps),
        exitBttn: fState.group === 'top' ? getExitButton(exitRootForm) : null
    };
    return _elems('getFormRows', [fState.name, fState.group])
        .then(addRowsAndReturnPieces);

    function addRowsAndReturnPieces(rows) {
        elems.rows = rows;
        return elems;
    }
}