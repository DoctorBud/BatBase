/**
 * Random methods that affect various form-elems.
 *
 * Export
 *     toggleSubmitBttn
 *     toggleFormStatusMsg
 *     toggleWaitOverlay
 */
import { _el } from '~util';
import * as status from './form-status-msg.js';
import * as toggle from './toggle-submit.js';

export function toggleSubmitBttn() {
    return toggle.toggleSubmitBttn(...arguments);
}
export function toggleFormStatusMsg() {
    status.toggleFormStatusMsg(...arguments);
}
export function toggleWaitOverlay(waiting) {
    if (waiting) { appendWaitingOverlay();
    } else { $('#c-overlay').remove(); }
}
function appendWaitingOverlay() {
    const attr = { class: 'overlay waiting', id: 'c-overlay'}
    $('#b-overlay').append(_el('getElem', ['div', attr]));
    $('#c-overlay').css({'z-index': '1000', 'display': 'block'});
}
