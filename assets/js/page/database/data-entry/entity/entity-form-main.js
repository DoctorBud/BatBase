/**
 * Entry point for entity-specific code.
 *
 * Note - required form methods: initCreateForm, initEditForm
 *
 * TOC
 *     INIT FORM
 *         IF OPEN SUB-FORM ISSUE
 *         FORM COMBOS
 *         EDIT FORMS
 *     ENTITY FACADE
 *         INTERACTION
 *         LOCATION
 *         TAXON
 *         SOURCE TYPES
 *             AUTHOR
 *             CITATION
 */
import * as build from './build/entity-build-main.js'
import * as confg from './confg/confg-main.js';
/* =================== FORM CONFG =========================================== */
export function _confg(funcName, params) {
    return confg[funcName](...params);
}
/* =================== INIT FORM ============================================ */
export function createEntity() {
    return build.createEntity(...arguments);
}
/* ------------------------- EDIT FORM -------------------------------------- */
export function editEntity() {
    return build.editEntity(...arguments);
}
/* =================== ENTITY FACADE ======================================== */
export function clearEntityFormMemory() {
    build.clearEntityFormMemory(...arguments);
}
/** ------------------------ INTERACTION ------------------------------------ */
export function fillCitationCombo() {
    build.fillCitationCombo(...arguments);
}
export function selectIntLoc(id) {
    build.selectIntLoc(id);
}
export function enableCountryRegionField() {
    build.enableCountryRegionField();
}
export function clearInteractionDate() {
    return build.clearInteractionDate(...arguments);
}
/** --------------------------- LOCATION ------------------------------------ */
export function addMapToLocForm(elem, mapType) {
    return build.addMapToLocForm(elem, mapType);
}
export function focusParentAndShowChildLocs(mapType, val) {
    return build.focusParentAndShowChildLocs(mapType, val);
}
export function autofillCoordinateFields() {
    build.autofillCoordinateFields(...arguments);
}
/** ------------------------ TAXON ------------------------------------------ */
export function selectFieldTaxon() {
    return build.selectFieldTaxon(...arguments);
}
export function enableTaxonFieldCombos() {
    return build.enableTaxonFieldCombos(...arguments);
}
export function getSelectedTaxon() {
    return build.getSelectedTaxon(...arguments);
}
export function initFieldTaxonSelect() {
    build.initFieldTaxonSelect(...arguments);
}
export function buildOptAndUpdateCombo() {
    return build.buildOptAndUpdateCombo(...arguments);
}
export function onTaxonFieldSelection() {
    return build.onTaxonFieldSelection(...arguments);
}
/** ------------------------ SOURCE TYPES ----------------------------------- */
export function finishSrcFieldLoad() {
    return build.finishSrcFieldLoad(...arguments);
}
/** ---------------- AUTHOR ------------------- */
/* edit-form, form-ui */
export function selectExistingAuthsOrEds() {
    return build.selectExistingAuthsOrEds(...arguments);
}
/** ---------- CITATION ------------------------- */
export function clearCitationCombo() {
    build.clearCitationCombo(...arguments);
}
export function handleCitText(formLvl) {
    build.handleCitText(formLvl);
}
export function setFinalCitation() {
    build.setFinalCitation(...arguments);
}