/**
 * Resets the interactions form leaving only the pinned values. Displays a
 * success message. Disables submit button until any field is changed.
 *
 * Export
 *     resetInteractionForm
 *
 * TOC
 *     HANDLE INTERACTION-FORM RESET
 *         HANDLE PERSISTED FIELDS
 *     CLEAR FIELD DATA
 *         CLEAR FIELD-TYPE
 *         FIELD-SPECIFIC CLEAR
 *     RESET FORM UI
 */
import { _cmbx } from '~util';
import { _elems, _form, _state } from '~form';
import * as iForm from '../int-form-main.js';

let fields;
/* ==================== HANDLE INTERACTION-FORM RESET ============================= */
export function resetInteractionForm() {
    _elems('toggleFormStatusMsg', ['New Interaction successfully created.']);
    resetIntFields();
    resetFormUi();
}
/**
 * Resets the top-form in preparation for another entry. Pinned field values are
 * persisted. All other fields will be reset.
 */
function resetIntFields() {
    fields = _state('getFormState', ['top', 'fields']);             /*dbug-log*///console.log('--resetInteractionForm fields[%O]', fields);
    Object.values(fields).map(handleFieldDataReset);
}
function handleFieldDataReset(field) {
    if (!field.shown) { return; }
    if (!$(`#${field.name}_pin`).prop('checked')) {
        clearField(field);
    } else {
        handePersistedField(field);
    }
}
/* --------------------- HANDLE PERSISTED FIELDS ---------------------------- */
function handePersistedField(field) {
    const map = {
        Date: setDateInitVal,
        InteractionTags: setFieldInitVal,
        InteractionType: setFieldInitVal
    }
    if (!map[field.name]) { return; }
    map[field.name](field);
}
/** Some fields will be set from a data-attr during another field's onChange. */
function setFieldInitVal(field) {
    $('#sel-'+field.name).data('init-val', field.value);
}
function setDateInitVal(field) {
    $(`#${field.id}-start`).data('init-val', field.value);
}
/* ==================== CLEAR FIELD DATA ==================================== */
function clearField(field) {                                        /*dbug-log*///console.log('--clearField field[%O]', _u('snapshot', [field]));
    // if (field.name === 'InteractionTags') { return handleClearedTags(field); } //DO NOT DELETE
    clearFieldValue(field);
    clearFieldType(field);
    if (field.type === 'textarea') { return  }
    handleClearedField(field);
}
/* ------------------- CLEAR FIELD-TYPE ------------------------------------- */
function clearFieldType(field) {
    const map = {
        page: clearTextareaField,
        select: clearComboField,
        fullTextArea: clearTextareaField
    };
    if (!map[field.type]) { return; }
    map[field.type](field);
}
function clearFieldValue(field) {
    $(`#${field.name}_pin`).prop('checked', false);
    field.value = null;
}
function clearComboField(field) {
    _cmbx('resetCombobox', [field.name]);
}
function clearTextareaField(field) {
    $(`#${field.name}_f .f-input`).val("");
}
/* -------------------- FIELD-SPECIFIC CLEAR -------------------------------- */
// /**
//  * Ensures default or required tags persist through field clear.
//  * NOTE: NOT USED CURRENTLY. STORED IN CASE THIS IS A HELPFUL FEATURE AGAIN.
//  */
// function handleClearedTypeTags(field) {
//     if (!field.value || !field.value.length) { return; }
//     const typeTags = fields.InteractionTags.misc.typeTags;
//     const clear = fields.InteractionTags.misc.defaultTags;          /*dbug-log*///console.log('--handleClearedTags current[%O] tags[%O]', _u('snapshot', [field.value]), clear);
//     const nTags = field.value.filter(i => !clear.find(t => t.value == i));/*dbug-log*///console.log('   new tags[%O]', nTags);
//     field.value = typeTags && typeTags.length > 1 ? [] : nTags;
//     _cmbx('setSelVal', [field.name, field.value, 'silent']);
// }
function handleClearedField(field) {
    const map = {
        CitationTitle: clearSidePanelCitationDetails,
        InteractionType: clearTypeAndTags,
        Location: syncWithCountryField,
        Object: clearTaxonField,
        Source: ensureQuoteIsNotRequiredAndPrimarySelected,
        Date: _form.bind(null, 'clearInteractionDate'),
        Publication: iForm.clearCitationCombo,
        Subject: clearTaxonField,
    }
    if (!map[field.name]) { return; }
    map[field.name](field);
}
function clearSidePanelCitationDetails(field) {
    if (!_cmbx('getSelVal', ['Publication'])) { return; } //already cleared
    _elems('clearSidePanelDetails', ['cit']);
}
function clearTypeAndTags(field) {
    iForm.onTypeSelection(null);
}
function clearTaxonField(field) {
    if (['Subject', 'Object'].indexOf(field.name) === -1) { return; }
    _cmbx('replaceSelOpts', [field.name, []]);
    _cmbx('enableCombobox', ['InteractionType', false]);
    $('#sel-'+field.name).data('selTaxon', false);
}
function syncWithCountryField(field) {
    const cntryId = fields['Country-Region'].value;
    const cntry = cntryId ? _state('getRcrd', ['location', cntryId]) : null;
    iForm.resetLocCombo(cntry);
    _elems('clearSidePanelDetails', ['loc']);
}
function ensureQuoteIsNotRequiredAndPrimarySelected() {
    _state('setFieldState', ['top', 'Quote', false, 'required']);
    _cmbx('setSelVal', ['Source', 'Primary']);
}
/* ==================== RESET FORM UI ======================================= */
function resetFormUi() {
    $('#top-cancel').val(' Close ');
    _elems('toggleSubmitBttn', ['top', false]);
    _state('setFormState', ['top', 'unchanged', true]);
}