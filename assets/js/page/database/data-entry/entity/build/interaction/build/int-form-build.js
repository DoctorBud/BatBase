/**
 * Inits the interaction form with all fields displayed and the first field,
 * publication, in focus. From within many of the fields the user can create
 * new entities of the field-type by selecting the 'add...' option from the
 * field's combobox and completing the appended sub-form.
 *
 * Export
 *     initCreateForm
 *     initEditForm
 *     finishInteractionFormBuild
 *
 * TOC
 *     INIT FORM
 *         CREATE
 *         EDIT
 *         SHARED
 *     FINISH BUILD
 *         REFERENCE-GUIDE BUTTON
 *         FORM COMBOBOXES
 *         ON-SUBMIT CONFIRMATION MODAL
 *     FILL INIT-DATA
 *         TAGS FIELDS
 *         TAXON FIELDS
 */
import { _cmbx } from '~util';
import { _elems, _state } from '~form';
import * as iForm from '../int-form-main.js';

/* ======================= INIT FORM ======================================== */
/* --------------------------- CREATE --------------------------------------- */
export function initCreateForm() {                            /*perm-log*/console.log('   //Building New Interaction Form');
    if (_state('getStateProp')) { return; } //Form is already opened.
    return _elems('initForm', [getIntFormParams('create')])
        .then(finishInteractionFormBuild);
}
/* ---------------------------- EDIT ---------------------------------------- */
export function initEditForm(entity, id) {                          /*perm-log*/console.log('   //Building EDIT Interaction Form id[%s]', id);
    return _elems('initForm', [getIntFormParams('edit', id)])
        .then(finishInteractionFormBuild)
        .then(finishFieldFill);
}
/* --------------------------- SHARED --------------------------------------- */
function getIntFormParams(action, id) {
    const onClose = action === 'create' ? iForm.resetInteractionForm : null;
    return {
        action: action,
        id: id,
        initCombos: iForm.initCombos,
        name: 'Interaction',
        onFormClose: onClose,
        // submit: showSubmitModal,
    };
}
/* ======================= FINISH BUILD ===================================== */
/**
 * Inits the selectize comboboxes, adds/modifies event listeners, and adds
 * required field elems to the form's config object.
 */
export function finishInteractionFormBuild() {                      /*dbug-log*///console.log('           --finishIntFormBuild');
    $('#txt-Note').change(iForm.focusPinAndEnableSubmitIfFormValid.bind(null, 'Note'));
    modifyFormDisplay();
    finishLocationInit();
    finishComboboxInit();
    iForm.initMiscFields();
}
function modifyFormDisplay() {
    _elems('setDynamicFieldStyles', ['interaction']);
}
/* -------------------------- FORM COMBOBOXES ------------------------------- */
function finishComboboxInit() {
    _cmbx('enableCombobox', ['CitationTitle', false]);
    iForm.addRoleTaxonFocusListeners();
    _cmbx('enableCombobox', ['InteractionType', false]);
    iForm.initTagField();
    focusPubFieldIfNewRecord();
}
function focusPubFieldIfNewRecord() {
    const action = _state('getFormState', ['top', 'action']);
    _cmbx('focusCombobox', ['Publication', action === 'create']);
}
/* ------------------------ LOCATION INIT ----------------------------------- */
function finishLocationInit() {
    iForm.addLocationSelectionMethodsNote();
    setDefaultLocationValue();
}
function setDefaultLocationValue() {
    const locField = _state('getFieldState', ['top', 'Location', null]);/*dbug-log*///console.log('   -- setDefaultLocationValue locField[%O]', locField)
    if (locField.value) { return; }
    locField.value = locField.misc.unspecified;
}
/* ====================== FILL INIT-DATA ==================================== */
function finishFieldFill() {
    const fields = _state('getFormState', ['top', 'fields']);       /*dbug-log*///console.log('--finishFieldFill [%O]', _u('snapshot', [fields]));
    setSourceFields(fields.Publication, fields.CitationTitle);
    setLocationDetailPanel(fields.Location.value);
    setTypeAndTagFields(fields.InteractionType, fields.InteractionTags);
    setTaxonFields(fields.Subject.value, fields.Object.value);
    if (fields.Source.value === 'Secondary') { setIntSecondarySource(); }
    if (fields.Season.value) { setIntSeasons(fields.Season.value); }

}
function setLocationDetailPanel(id) {
    const locRcrd = _state('getRcrd', ['location', id]);
    _elems('setSubEntityDetails', ['loc', locRcrd]);
}
function setSourceFields(pubField, citField) {                      /*dbug-log*///console.log('--setSourceFields pub[%O] cit[%O]', pubField, citField);
    iForm.fillCitationCombo(pubField.value);
    _cmbx('setSelVal', ['CitationTitle', citField.value]);
}
/* ------------------------ TAGS FIELDS ------------------------------------- */
function setTypeAndTagFields(typeField, tagsField) {
    $('#sel-InteractionType').data('init-val', typeField.value);
    sortAndSetTagsFields(tagsField);
}
/** Sorts through tags by type and sets their respective fields. */
function sortAndSetTagsFields(tagsField) {
    const tags = sortTagsByType(tagsField.value, _state('getEntityRcrds', ['tag']));
    if (tags.source) { setIntSecondarySource(); }
    if (tags.season) { setIntSeasons(tags.season); }                /*dbug-log*///console.log('sortAndSetTagsFields tags[%O]', tags);
    if (!tags['interaction type']) { return; }
    /** Note: Must happen before taxon fields filled for auto-type handling. */
    $('#sel-InteractionTags').data('init-val', tags['interaction type']);
}
function setIntSecondarySource() {
    _cmbx('setSelVal', ['Source', 'Secondary']);
}
function setIntSeasons(seasonIds) {
    _cmbx('setSelVal', ['Season', seasonIds]);
}
function sortTagsByType(tagIds, tagRcrds) {                         /*dbug-log*///console.log('sortTagsByType tagIds[%O] tagRcrds[%O]', tagIds, tagRcrds);
    const sorted = {};
    tagIds.forEach(sortTag);
    return sorted;

    function sortTag(id) {
        const type = tagRcrds[id].type;
        if (!sorted[type]) { sorted[type] = []; }
        sorted[type].push(id);
    }
}
/* ------------------------ TAXON FIELDS ------------------------------------ */
function setTaxonFields(subId, objId) {
    iForm.buildOptAndUpdateCombo('Subject', subId);
    iForm.buildOptAndUpdateCombo('Object', objId);
}
