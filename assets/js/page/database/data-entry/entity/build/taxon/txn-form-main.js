/**
 * Taxon create|edit and select -form facade.
 */
import * as create from './create/txn-create-form.js';
import * as edit from './edit/txn-edit-main.js';
import * as select from './select/txn-select-main.js';

/* ======================= CREATE =========================================== */
export function initCreateForm() {
    return create.initCreateForm(...arguments);
}
/* ========================= EDIT =========================================== */
export function initEditForm() {
    return edit.initEditForm(...arguments);
}
/* ======================= SELECT =========================================== */
export function initFieldTaxonSelect() {
    select.initFieldTaxonSelect(...arguments);
}
export function initSelectFormCombos() {
    return select.initSelectFormCombos(...arguments);
}
export function getSelectedTaxon() {
    return select.getSelectedTaxon(...arguments);
}
/* ======================= VALIDATION ======================================= */
export function ifSpeciesValIssue() {
    return create.ifSpeciesValIssue(...arguments);
}
export function ifMissingParentTaxon() {
    return create.ifMissingParentTaxon(...arguments);
}