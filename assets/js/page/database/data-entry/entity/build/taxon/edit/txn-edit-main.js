/**
 * Taxon edit-form.
 *
 * TOC
 *     CORE FIELDS
 *         NAME FIELD AND RANK COMBOBOX
 *         PARENT TAXON NAME AND CHANGE BUTTON
 *     TAXON PARENT SELECT FORM
 *         RANK COMBO ELEMS
 *         FINISH SELECT FORM BUILD
 *         DATA VALIDATION
 *     ROW BUILDERS
 *     FINISH EDIT FORM BUILD
 *     DATA VALIDATION
 *         RANK VALIDATION
 *             RANK NOT AVAILABLE IN NEW GROUP
 *             MUST REMAIN GENUS
 *             NEEDS HIGHER RANK
 *         ALERTS
 */
import { _cmbx } from '~util';
import { _state, _elems, _form, _val } from '~form';

/* ========================= INIT FORM ====================================== */
export function initEditForm(entity, id) {                          /*perm-log*/console.log('           >--EDIT [%s][%s]', entity, id);
   const p = getEditFormParams(id);                                 /*dbug-log*///console.log('   --params[%O]', p);
    return _elems('initForm', [p])
        .then(finishFormInit);
}
/* -------------------------- PARAMS ---------------------------------------- */
function getEditFormParams(id) {                                    /*dbug-log*///console.log('--getEditFormParams id[%s]', id);
    return {
        action: 'edit',
        group: 'top',
        id: id,
        initCombos: initCombos,
        name: 'Taxon',
        style: 'sml-form',
        type: 'edit'
    };
}
function initCombos() {
    const events = {
        Rank: { onChange: onRankChangeValidate },
        Parent: { onChange: onParentChange },
    };
    _elems('initFormCombos', ['top', events]);
}
/* ------------------------- FINISH BUILD ----------------------------------- */
function finishFormInit(status) {                                   /*dbug-log*///console.log('--finishFormInit');
    if (!status) { return; } //Error handled elsewhere
    const fields = _state('getFormState', ['top', 'fields']);
    handleParentTaxonInit(fields.Parent.value);
}
/** ======================= RANK ============================================ */
/**
 * Ensures that the new taxon-rank is higher than its children, and that a
 * species taxon being edited has a genus parent selected.
 */
function onRankChangeValidate(val) {                                   /*dbug-log*///console.log("--onRankChangeValidate val[%s]", val);
    const valData = buildRankValData();
    validateRank(valData);
}
/* ---------------- BUILD RANK VALIDATION DATA ------------------------------ */
function buildRankValData() {
    const data = _state('getEntityRcrds', [['taxon', 'orderedRanks']]);/*dbug-log*///console.log('    --buildRankValData data[%O]', data);
    data.rcrd = _state('getFormState', ['top', 'rcrd']);
    data.newRank = _cmbx('getSelTxt', ['Rank']);
    data.childRank = getHighestChildRank(data.rcrd, data.taxon, data.orderedRanks);
    data.parentRank = getParentRank(data.rcrd);
    return data;

    function getParentRank(rcrd) {
        const newRank = _state('getFieldState', ['top', 'Parent', 'misc']);
        return newRank ? newRank : data.taxon[rcrd.parent].rank.displayName;
    }
}
function getHighestChildRank(taxon, taxa, ranks) {
    let high = ranks.indexOf('Species');
    taxon.children.forEach(checkChildRank);
    return ranks[high];

    function checkChildRank(id) {
        const childIdx = ranks.indexOf(taxa[id].rank.displayName);
        if (childIdx <= high) { return; }
        high = childIdx;
    }
}
/* -------------------------- RANK VALIDATION ------------------------------- */
function validateRank(data) {                                       /*dbug-log*///console.log("--validateRank data[%O]", data);
    const issues = {
        isGenusPrnt: data.childRank === 'Species' && data.newRank !== 'Genus',
        needsHigherRank: ifRankTooLow(data.newRank, data.childRank, data.orderedRanks),
        needsLowerRank: ifRankTooLow(data.parentRank, data.newRank, data.orderedRanks)
    };                                                              /*dbug-log*///console.log('   --issues[%O]', issues);
    for (let tag in issues) {
        if (issues[tag]) { return shwTxnValAlert(tag, 'Rank', 'top'); }
    }
    clearActiveAlert('Rank');
}
/* -------- NEEDS HIGHER RANK -------------------- */
function ifRankTooLow(highRank, lowRank, ranks) {                    /*dbug-log*///console.log('  --ifRankTooLow? high[%s] <= low[%s]', highRank, lowRank);
    return ranks.indexOf(highRank) <= ranks.indexOf(lowRank);
}
/** ======================= PARENT TAXON ==================================== */
/* -------------------------- INIT ------------------------------------------ */
function handleParentTaxonInit(pId) {                               /*dbug-log*///console.log('--handleParentTaxonInit');
    $(`#sel-Parent`)[0].selectize.on('focus', loadParentSelectForm);
    $('#sel-Parent').data('selTaxon', pId);
    _form('buildOptAndUpdateCombo', ['Parent', pId, 'silent']);
}
function loadParentSelectForm() {
    const gId = _state('getFieldState', ['top', 'Group']);
    const sgId = _state('getFieldState', ['top', 'Sub-Group']);     /*dbug-log*///console.log('--loadParentTaonxSelectForm g[%s] sg[%s]', gId, sgId);
    _form('initFieldTaxonSelect', ['Parent', gId, sgId, onParentChange]);
}
/* -------------------------- ON CHANGE ------------------------------------- */
/**
 * @param  {int|obj} val   Either an ID or a jQuery event obj.
 */
function onParentChange(val) {
    const pTxn = getNewParentRecord(val);                           /*dbug-log*///console.log("--onParentChange pTxn[%O]", pTxn);
    const valData = buildParentValData(pTxn);
    if (!validateParent(valData)) { return; } //Issue alert shown
    _form('buildOptAndUpdateCombo', ['Parent', pTxn.id, 'silent']);
    _form('onTaxonFieldSelection', ['Parent', pTxn.id]);
    updateGroupState(pTxn);
    _elems('checkReqFieldsAndToggleSubmitBttn', ['top']);
}
function getNewParentRecord(val) {
    return isNaN(val) ? _form('getSelectedTaxon') : _state('getRcrd', ['taxon', val]);
}
function updateGroupState(pTxn) {
    _state('setFieldState', ['top', 'Parent', pTxn.id ]);
    _state('setFieldState', ['top', 'Parent', pTxn.rank.displayName, 'misc' ]);
    _state('setFieldState', ['top', 'Group', pTxn.group.id ]);
    _state('setFieldState', ['top', 'Sub-Group', pTxn.group.subGroup.id ]);
}
/* ------------------------ PARENT VALIDATION ------------------------------- */
function buildParentValData(pTxn) {
    const data = _state('getEntityRcrds', [['orderedRanks']]);      /*dbug-log*///console.log('    --buildParentValData data[%O]', data);
    data.rcrd = pTxn;
    data.newRank = pTxn.rank.displayName;
    data.childRank = _cmbx('getSelTxt', ['Rank']);
    data.parent
    return data;
}
function validateParent(data) {                                     /*dbug-log*///console.log("--validateParent data[%O]", data);
    const issues = {
        rankNotAvailableInNewGroup: ifInvalidGroupRank(data.childRank),
        needsHigherRank: ifRankTooLow(data.newRank, data.childRank, data.orderedRanks),
        needsGenusPrnt: ifSpeciesParentMustBeGenus(data.newRank, data.childRank)
    };                                                              /*dbug-log*///console.log('   --issues[%O]', issues);
    for (let tag in issues) {
        if (issues[tag]) { return shwTxnValAlert(tag, 'Parent', 'top'); }
    }
    clearActiveAlert('Parent');
    return true;
}
function ifSpeciesParentMustBeGenus(pRank, txnRank) {
    return txnRank === 'Species' && pRank !== 'Genus';
}
/* -------- RANK NOT AVAILABLE IN NEW GROUP ---------------- */
function ifInvalidGroupRank(txnRank) {
    const sgField = _state('getFieldState', ['sub', 'Sub-Group', 'misc']);/*dbug-log*///console.log('--ifInvalidGroupRank? sgField[%O]', sgField);
    return sgField.subRanks.indexOf(txnRank) === -1;
}
/* ------------------------- ALERTS ----------------------------------------- */
function clearActiveAlert(field) {                                  /*dbug-log*///console.log('--clearActiveAlert field[%s]', field);
    if (!$('.top-active-alert').length) { return; }
    _val('clearAlert', [ $(`#${field}_alert`)[0], 'top' ]);
}
function shwTxnValAlert(tag, field) {
    _val('showFormValAlert', [field, tag, 'top']);
    _elems('toggleSubmitBttn', ['top', false]);
    return false;
}