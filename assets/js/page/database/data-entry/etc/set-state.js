/**
 * TODO: DOCUMENT
 *
 * Export
 *
 * TOC
 *
 */
import { _elems } from '~form';
/* ======================= SET CORE STATE =================================== */
export function setStateProp(fS, prop, val) {                       /*dbug-log*///console.log('   --setStateProp [%s][%s][%O]', prop, val, fS);
    fS[prop] = val;
}
/**
 * Edge case: After form submit, the updated data is fetched and stored here, but
 * if the form is closed before the data is stored, cancel storing the data.
 */
export function setEntityRecords(fS, entity, rcrds) {               /*dbug-log*///console.log('   --setEntityRecords entity[%s] rcrds[%O] fS[%O]', entity, rcrds, fS);
    fS.records[entity] = rcrds;
}
export function clearSubFormState(fS, fLvl) {                       /*dbug-log*///console.log('   --clearSubFormState [%s][%O]', fLvl, fS);
    const fState = fS.forms[fLvl];
    delete fS.forms[fState.name];
    delete fS.forms[fLvl];
    _elems('onSubFormCloseUpdateReviewPanel', [fLvl]);
}
/* ====================== SET FORM STATE ==================================== */
export function setFormState(fState, prop, val) {                   /*dbug-log*///console.log('   --setFormState [%s][%s][%O]', prop, val, fState);
    fState[prop] = val;
}
/* ----------------------- FIELDS ------------------------ */
export function setFieldState(fState, field, val, prop = 'value') { /*dbug-log*///console.log('    --setFieldState fields[%s] prop[%s] val?[%O] [%O]', field, prop, val, fState);//console.trace()
    let fData = fState.fields[field];
    if (!prop) { fData = val;
    } else { fData[prop] = val }
    onFieldStateChange(fState, field, val, prop);
}
function onFieldStateChange(fState, field, val, prop) {
    const map = {
        required: toggleRequiredField
    };
    if (!prop) { toggleRequiredField(fState, field, val.required); }
    return map[prop] ? map[prop](...arguments) : null;
}
function toggleRequiredField(fState, field, val) {                  /*dbug-log*///console.log('    --toggleRequiredField fields[%s] val?[%O] [%O]', field, val, fState);//console.trace()
    if (val) {
        $(`#${field}_lbl`).addClass('required');
        _elems('checkReqFieldsAndToggleSubmitBttn', [fState.group]);
    } else {
        $(`#${field}_lbl`).removeClass('required');
    }
}
/* ___________________________ TAXON ________________________________________ */
/** Note: Group init-value required. */
export function setTaxonGroupState(rcrds, f) {                      /*dbug-log*///console.log('--setTaxonGroupState rcrds[%O] f[%O]', rcrds, f);
    const group = getGroupEntity(rcrds, f);
    const sGroupId = getSubGroupId(rcrds, f, group);                /*dbug-log*///console.log('   --setTaxonGroupState subGroupId[%s] group[%O] ', sGroupId, group);
    setGroupState(rcrds.taxon, f, group);
    setSubGroupState(rcrds.taxon, f.fields, group.subGroups, sGroupId);
}
function getGroupEntity(rcrds, f) {
    return rcrds.group[f.fields.Group.value];
}
function getSubGroupId(rcrds, f, group) {
    const subGroupId = f.fields['Sub-Group'].value;                 /*dbug-log*///console.log('--getSubGroupId [%s]', subGroupId);
    return subGroupId ? subGroupId : Object.keys(group.subGroups)[0];
}
/** [setGroupState description] */
function setGroupState(taxa, fState, group) {                       /*dbug-log*///console.log('--setGroupState group[%O]', group);
    fState.fields.Group.value = group.id;
    fState.fields.Group.misc = {
        rcrd: group,
        subGroups: group.subGroups
    };                                                              /*dbug-log*///console.log('   --updated state[%O]', _u('snapshot', [fState.fields.Group.misc]));
}
/** [setSubGroupState description] */
function setSubGroupState(rcrds, fields, subGroups, sGroupId) {     /*dbug-log*///console.log('--setSubGroupState rcrds[%O], fields[%O], subGroups[%O], sGroupId[%s]', rcrds, fields, subGroups, sGroupId);
    const subGroup = subGroups[sGroupId];
    fields['Sub-Group'].shown = Object.keys(subGroups).length > 1;
    fields['Sub-Group'].misc = {
        rcrd: subGroup,
        subRanks: subGroup.subRanks,
        taxon: rcrds[subGroup.taxon]
    };
    fields['Sub-Group'].value = subGroup.id;                        /*dbug-log*///console.log('--setSubGroupState field[%O] subGroup[%O] subGroups[%O]', _u('snapshot', [fields['Sub-Group']]), subGroup, subGroups);
}