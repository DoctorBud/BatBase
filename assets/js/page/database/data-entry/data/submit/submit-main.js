/**
 * Handles data-submit chain.
 *
 * Export
 *     submitForm
 *
 * TOC
 *     SUBMIT FORM
 *         DATA PREPARATION
 *         PUSH DATA
 *         AFTER SUBMIT
 */
import { _u } from '~util';
import { _data, _elems, _form, _state, _val } from '~form';
import syncLocalDataAndUpdateForm from './after-submit.js';
/** ======================= DATA PREPARATION ================================ */
export function submitForm(fLvl) {
    _elems('toggleWaitOverlay', [true]);
    _elems('toggleSubmitBttn', [fLvl, false]);
    onceFormDataReadyHandleSubmit(_state('getFormState', [fLvl]));
}
function onceFormDataReadyHandleSubmit(confg) {
    if (confg.name === 'Citation') { //Ensure citation-text has been generated
        return window.setTimeout(() => prepCitationDataAndSubmit(confg), 1000);
    }
    prepDataAndHandleSubmit(confg);
}
function prepCitationDataAndSubmit(confg) {
    _form('setFinalCitation', [confg.group]);
    prepDataAndHandleSubmit(confg);
}
/** ----------------------- DATA PREPARATION -------------------------------- */
function prepDataAndHandleSubmit(confg) {
    _data('getValidatedFormData', [confg])
    .then(data => handleFormSubmit(data, confg));
}
function handleFormSubmit(fData, confg) {
    if (fData.fails) { return handleDataPrepFailure(fData.fails); } /*dbug-log*///console.log("   --submitFormData fData[%O] confg[%O]", fData, confg);
    if (_u('getUserRole') !== 'contributor') { return pushDataEntry(fData, confg); }
    pushDataForReview(fData, confg);
}
function handleDataPrepFailure(fails) {
    if (fails === 'handled') { return; }
    _val('errUpdatingData', ['dataPrepFail', fails]);
}
/** ========================== PUSH DATA ==================================== */
function pushDataEntry(data, confg) {
    const url = `crud/entity/${confg.action}`;
    pushData(data, url, confg.group);
}
/** -------------------- QUARANTINE DATA ------------------------------------ */
function pushDataForReview(data, confg) {
    const url = `crud/entity/pend`;
    data.pend = getDataToQuarantine(confg);
    pushData(data, url, confg.group);
}
function getDataToQuarantine(confg) {
    return {
        action: confg.action,
        stage: 0, //0: Pending, 1: Locked, 2: Returned, 3: Held
        notes: $('#rvw-msg').val(), //Notes from contributor to manager (eventually editor to editor+)
        data: JSON.stringify({
            values: _state('getFieldValues', [confg.group]),
        })
    };
}
/** ---------------------------- PUSH --------------------------------------- */
function pushData(data, url, fLvl) {
    _u('sendAjaxQuery', [data, url, pushSuccess, pushError]);                   _u('logInProdEnv', ['data = ', JSON.stringify(data)]);

    // Shows alerts from server errors
    function pushError() {
        _elems('toggleWaitOverlay', [false]);
        _val('formSubmitError', [fLvl, ...arguments]);
    }
    function pushSuccess(data) {                                                _u('logAjaxData', [data, arguments]);
        if (data.results.error) { return pushError(null, data.results.error); }
        // Handles errors with entity validation
        if (data.results.fails) { return _val('showServerValAlert', [fLvl, data.results.fails]); }
        syncLocalDataAndUpdateForm(fLvl, data.results)
        .then(() => _elems('toggleWaitOverlay', [false]));
    }
}