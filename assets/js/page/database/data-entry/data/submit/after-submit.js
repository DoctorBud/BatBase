/**
 * After form-submit-success, local data is updated and the form-process continues.
 *
 * Export
 *     onSubmitSuccess
 *
 * TOC
 *     ON SUBMIT SUCCESS
 *         TOP-FORM
 *         SUB-FORM
 */
import { _cmbx, _db, _u } from '~util';
import { _state, _elems, _val, clearFormMemory } from '~form';
/* =================== ON SUBMIT SUCCESS ==================================== */
export default function syncLocalDataAndUpdateForm(fLvl, data) {
    const confg = _state('getFormState', [fLvl]);
    confg.submitted = true; //Triggers special-handling on root-form exit
    return _db('afterDataEntrySyncLocalDatabase', [data, confg])
        .then(data => onDataSynced(fLvl, data));
}
function onDataSynced(fLvl, data) {                                 /*dbug-log*///console.log('       --onDataSynced [%s][%O]', fLvl, data);
    if (!_state('getStateProp')) { return; } //form closed.
    if (data.fails) { return _val('errUpdatingData', ['dataSyncFailures', data.fails]); }
    if (noDataChanges()) { return showNoChangesMessage(); }
    return addDataToStoredRcrds(data.core, data.detail)
    .then(() => handleFormComplete(fLvl, data));

    function noDataChanges() {
        const action = _state('getFormState', [fLvl, 'action'])
        return action === 'edit'  && !hasChngs(data);
    }
}
/**
 * Returns true if there have been user-made changes to the entity.
 * Note: The location elevUnitAbbrv is updated automatically for locations with elevation data, and is ignored here.
 */
function hasChngs(data) {
    const chngs = Object.keys(data.coreEdits).length > 0 ||
        Object.keys(data.detailEdits).length > 0;
    if (ifLocElevUnitIsTheOnlyChange(data.core)) { return false; }
    return chngs;

    function ifLocElevUnitIsTheOnlyChange() {
        const editCnt = Object.keys(data.coreEdits).length;
        return chngs && data.core == 'location' && editCnt == 2
            && 'elevUnitAbbrv' in data.coreEdits;
    }
}
function showNoChangesMessage() {
    _elems('toggleFormStatusMsg', ['No changes detected.', 'red']);
}
/** Updates the core records in the global form params object. */
function addDataToStoredRcrds(entity, detailEntity) {               /*dbug-log*///console.log('--addDataToStoredRcrds. [%s] (detail ? [%s])', entity, detailEntity);
    return _db('getData', [_u('lcfirst', [entity])])
    .then(addDataToMemory);

    function addDataToMemory(data) {                                /*dbug-log*///console.log('   --addDataToMemory data[%O]', data);
        _state('setEntityRecords', [entity, data]);
        if (detailEntity) { return addDataToStoredRcrds(detailEntity); } //Source & Location's detail entities: publications, citations, authors, geojson
    }
}
/* --------------------- TOP FORM ------------------------------------------- */
function handleFormComplete(fLvl, data) {                           /*dbug-log*///console.log('--handleFormComplete fLvl = ', fLvl);
    if (fLvl !== 'top') { return exitFormAndSelectNewEntity(data, fLvl); }
    const onClose = _state('getFormState', ['top', 'onFormClose']); /*dbug-log*///console.log('   --onClose = %O', onClose);
    if (onClose) { onClose(data);
    } else { _elems('exitRootForm'); }
}
/* --------------------- SUB FORM ------------------------------------------- */
/**
 * Exits the successfully submitted form @exitForm. Adds and selects the new
 * entity in the form's parent elem @addAndSelectEntity.
 */
function exitFormAndSelectNewEntity(data, fLvl) {                    /*dbug-log*///console.log('           --exitFormAndSelectNewEntity.');
    const comboField = _state('getFormState', [fLvl, 'combo']);
    _elems('exitSubForm', [fLvl]);
    if (comboField) { addAndSelectEntity(data.coreEntity, comboField);
    } else { clearFormMemory(); }
}
/** Adds and option for the new entity to the form's parent elem, and selects it. */
function addAndSelectEntity(entity, comboField) {
    const newOpt = { text: entity.displayName, value: entity.id };
    _cmbx('addOpt', [comboField, newOpt]);
    _cmbx('setSelVal', [comboField, entity.id]);
}