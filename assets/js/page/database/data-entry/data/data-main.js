/**
 * Entry-point for data-submit (format, push/quarantine, sync) and -review chains.
 *
 * TOC
 *     EDIT FORM-DATA
 *     SUBMIT FORM-DATA
 */
import * as edit from './edit/edit-data.js';
import * as submit from './submit/submit-main.js';
import * as validate from './validate/data-validate-main.js';

/** ------------------------- EDIT FORM-DATA -------------------------------- */
export function setEditFieldValues() {
    return edit.setEditFieldValues(...arguments);
}
/** ----------------------- SUBMIT FORM-DATA -------------------------------- */
export function submitForm() {
    submit.submitForm(...arguments);
}
/** ------------------------ REVIEW FORM-DATA ------------------------------- */
// None for now
/** ----------------------- VALIDATE FORM-DATA ------------------------------ */
export function getValidatedFormData() {
    return validate.getValidatedFormData(...arguments);
}