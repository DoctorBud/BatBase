/**
 * Module facade for data validation that happens on form-submit
 *
 * Export
 *     getValidatedFormData
 */
import wrangleFormData from './format-data.js';
import * as entity from './entity/validate-entity-main.js';
/** ----------------------- VALIDATE FORM-DATA ------------------------------ */
/**
 * Starts the data-wrangling - sends validation module to the formatting module.
 * @return {obj} Data validated and formatted for the server.
 */
export function getValidatedFormData() {
    const entityVal = entity.getEntityValidationApi();
    return wrangleFormData(entityVal, ...arguments);
}