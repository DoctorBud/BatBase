/**
 * Location submit-data validation methods.
 *
 * Export
 *
 * TOC
 */

/** Fail prop is checked when returned and an alert is shown to the user. */
function returnFailResults() {
    return { fail: { name: '' } };
}