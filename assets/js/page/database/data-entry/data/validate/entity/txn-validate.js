/**
 * Taxon submit-data validation methods.
 *
 * Export
 *     verifyTaxonym
 *
 * TOC
 *     VERIFY TAXONYM
 */
import { _modal } from '~util';
import { _state } from '~form';

/** ================== VERIFY TAXONYM ======================================= */
/**
 * Verifys taxonym using the Global Name verifier.
 * NOTE: CURRENTLY DISABLED UNTIL IT'S TIME TO SET UP THE ADVANCED/FACETED SEARCH
 * TO HANDLE SPECIES TAXONYMS.
 * @param  {obj} fConfg  Name field-confg.
 * @param  {obj} fState  Form state/config
 * @return {prm}         Promise: fetch then handle verification-results
 */
export function verifyTaxonym(fConfg, fState) {                     /*dbug-log*///console.log('  --verifyTaxonym val[%s] f[%O] fState[%O]', fConfg.value, fConfg, fState);
    return fetch(buildTaxonymVerificationUrl(fConfg))
        .then(res => res.json())//response type
        .then(handlVerificationResults.bind(null, fState));
}
function buildTaxonymVerificationUrl(fConfg) {
    let url = 'https://verifier.globalnames.org/api/v0/verifications/';
    url += fConfg.value.split(' ').join('+');                       /*dbug-log*///console.log('  -- buildTaxonymVerificationUrl url[%s]', url);
    return url;
}
/** ---------------------- HANDLE RESULTS ----------------------------------- */
/**
 * Handles the taxonym verification results.
 * Results schema: https://apidoc.globalnames.org/gnames-beta#/default/get_verifications__names_
 * @param  {json}  results  JSON results returned from the global names verifier
 */
function handlVerificationResults(fState, results) {                /*temp-log*/console.log('  -- handlVerificationResults [%s]results[%O]', results.names[0].matchType, results);
    const handler = getMatchTypeHandlers(results.names[0].matchType);
    return handler(results.names[0], fState);
}
/**
 * Return the handler for the type of match results.
 * Match Types:
    NoMatch (no matched name found)
    PartialFuzzy (fuzzy partial match after removing some parts)
    PartialExact (match after removing last or middle epithets)
    Fuzzy (fuzzy match to a canonical form)
    Exact (exact match to a canonical form or a verbatim string)
    Virus (literal match of Viruses, plasmids, prions and other non-cellular entities)
    FacetedSearch (match by a faceted search: advanced query results) https://github.com/gnames/gnverifier#advanced-search
 * @param  {str} type   Match result-type
 */
function getMatchTypeHandlers(type) {                               /*dbug-log*///console.log('  -- getMatchTypeHandlers  type[%s]', type);
    const map = {
        NoMatch: () => returnFailResults(),
        PartialFuzzy: () => showModalConfirmation.bind(null, type),
        PartialExact: () => showModalConfirmation.bind(null, type),
        Fuzzy: () => showModalConfirmation.bind(null, type),
        Exact: () => console.log('TAXONYM VERIFIED'),
        Virus: () => console.log('VIRUS TAXONYM VERIFIED'),
        FacetedSearch: () => console.log('HANDLE '+type)
    };
    return map[type];
}
/** Fail prop is checked when returned and an alert is shown to the user. */
function returnFailResults() {
    return { fail: { name: 'Taxonym' } };
}
/** --------------------------- CONFIRM MODAL ------------------------------- */
function showModalConfirmation(type, name, fLvl) {                  /*temp-log*/console.log('  -- showModalConfirmation [%s] name[%O]', name.name, name);
    let resolve, reject;
    _modal('showSaveModal', [ buildModalConfg(name) ]);
    $(`#top-submit`).css({opacity: .5, cursor: 'not-allowed'})
    window.setTimeout(() => $('.modal-msg').css({width: 'max-content'}), 500);
    return new Promise((toResolve, toReject) => { resolve = toResolve; reject = toReject; })

    function buildModalConfg(name) {
        return {
            html: buildTaxonymConfirmationMsg(name),
            selector: `#top-delete`,
            dir: getTaxonymModalDir(),
            submit: acceptSuggestion,
            cancel: exitSuggestion,
            bttn: 'CONFIRM'
        };
    }
    function acceptSuggestion() {
        _state('setFieldState', [fLvl, 'DisplayName', name.name]);
        resolve({ newValue: name.name });
    }
    function exitSuggestion() {
        reject();
    }
}
function buildTaxonymConfirmationMsg(name) {                        /*dbug-log*///console.log('  -- buildTaxonymConfirmationMsg name[%O]', name);
    let str = 'Unverified taxonym. Would you like to accept this alternative?<br><br>';
    str += `${name.name}<br>`;
    if (name.classificationPath) { str + name.classificationPath; }
    return str;
}
function getTaxonymModalDir() {
    const forms = _state('getStateProp', ['forms']);
    return Object.keys(forms).indexOf('Object') !== -1 ? 'left' : 'right';
}