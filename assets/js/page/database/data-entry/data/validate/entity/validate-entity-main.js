/**
 * Module facade for entity-data validation.
 *
 * Export
 *
 * TOC
 *     
 */
// import * as loc from './txn-validate.js';
// import * as src from './txn-validate.js';
import * as txn from './txn-validate.js';

/**
 * NOTE: Must return a promise.
 * @return {obj}  Method-map of all entity-validation.
 */
export function getEntityValidationApi() {
    return {
        taxon: {
            verifyTaxonym: (fConfg, fState) => txn.verifyTaxonym(fConfg, fState),
        }
    };
}