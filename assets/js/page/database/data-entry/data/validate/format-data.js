/**
 * Returns an object with (k) the form field and (v) value.
 * TODO: DOCUMENT
 *
 * Export
 *     wrangleFormData
 *
 * TOC
 *     SERVER-DATA OBJ
 *         INIT
 *         RETURN
 *     WRANGLE DATA
 *         WRANGLE FIELD
 *         GET DATA
 *         SET DATA
 *             GENERAL
 *             SPECIFIC
 *             ENTITY
 *                 AUTHOR
 *                 CITATION
 *                 INTERACTION
 *                 LOCATION/GEOJSON
 *                 TAXON
 *     QUARANTINED DATA
 *     TRACK FAILURES
 */
import { _alert, _cmbx, _u } from '~util';
import { _state, _val } from '~form';
/**
 * Local module data.
 * @type  {obj}
 * @param {obj} data      Data prepared to be pushed to the server database.
 * @param {obj} validate  Data-validation module.
 * @param {ary} waiting   Array of fields with incomplete async methods.
 */
let ld;

export default function wrangleFormData(validate, confg) {
    setLocalModuleData(validate, confg);
    initServerDataObj(confg);                                       /*dbug-log*///console.log('+--getValidatedFormData. name[%s] ld[%O]', confg.name, ld);
    return Promise.all(wrangleAllFormData())
        .then(waitForAllFieldAsyncMethods)
        .then(alertIfFailures)
        .then(returnServerData);
}
function waitForAllFieldAsyncMethods() {
    return Promise.all(ld.waiting.map(e => e.async));
}
/* ------------------ LOCAL-MODULE DATA ------------------------------------- */
/**
 * Data available through the current file-scope. Emptied before data is returned.
 */
function setLocalModuleData(validate, confg) {
    initLocalModuleDataObj();
    if (confg.editing) { ld.data.ids = confg.editing; }
    ld.confg = confg;
    ld.validate = validate;
}
function initLocalModuleDataObj() {
    ld = { data: {}, waiting: [] };
}
/* =================== SERVER-DATA OBJ ====================================== */
/** [buildServerDataObj description] */
function initServerDataObj(c) {                                     /*dbug-log*///console.log('   --initServerDataObj c[%O]', c);
    const entity = c.core ? c.core : c.name;
    initEntityDataObj('core', entity);
    if (!c.core) { return; }
    initDetailDataObj(c.name);
}
/* ------------------------- INIT ------------------------------------------- */
function initEntityDataObj(dKey, name) {
    if (ld.data[dKey]) { return; }
    ld.data[dKey] = { flat: {}, rel:{} };
    ld.data[dKey+'Entity'] = _u('lcfirst', [name]);
}
function initDetailDataObj(name) {
    const entity = name ? name : 'GeoJson';
    initEntityDataObj('detail', entity);
}
/* ------------------------- RETURN ----------------------------------------- */
function returnServerData() {
    const sData = _u('snapshot', [ld.data]);
    initLocalModuleDataObj();
    return sData;
}
/* ========================= WRANGLE DATA =================================== */
function wrangleAllFormData() {
    return Object.values(ld.confg.fields).map(wrangleField);
}
/** Wranges the form-data into data prepared and sanitized for the server. */
function wrangleField(fConfg) {                                     /*dbug-log*///console.log('       --wrangleField [%s][%O]', fConfg.name, fConfg);
    const fKey = fConfg.entity ? 'rel' : 'flat';
    if (!fConfg.active) { return; }  //Field not active. TODO: if field had data before edit began, set field "null" here
    const v = getFieldValue(fConfg);
    if (!v) { return handleEmptyFieldData(fConfg); }
    if (fConfg.prep) { return handleDataPreparation(fKey, fConfg);  }
    setServerData(fKey, getFieldPropName(fConfg, fKey), v);
}
function handleEmptyFieldData(fConfg) {
    if (!fConfg.required) { return; }
    trackFailure(fConfg.name, fConfg.value);
}
function getFieldPropName(fConfg, fKey) {
    return fKey === 'rel' ? fConfg.entity : fConfg.name;
}
/* ----------------------- GET DATA ----------------------------------------- */
function getFieldValue(fConfg) {
    if (fConfg.value === undefined) { return null; } //Field never set
    if (fConfg.type === 'multiSelect') { return !!Object.keys(fConfg.value).length; }
    if (!_u('isObj', [fConfg.value])) { return fConfg.value; }
    return fConfg.value.value; //combos
}
/* ----------------------- VALIDATE  DATA ----------------------------------- */
/** Checks if this field prep-method is present in the entity-validation module. */
function isValidationMethod(fConfg, handler) {
    const entity = getFieldDataEntity(fConfg);
    return entity && ld.validate[entity] && ld.validate[entity][handler];
}
function getFieldDataEntity(fConfg) {
    const entity = fConfg.entity ? fConfg.entity : ld.confg.entity; /*dbug-log*///console.log('  --getFieldDataEntity entity?[%O]', entity);
    return entity ? _u('lcfirst', [entity]) : false;
}
/** Calls the method from the validation-module and handles those results.
 */
function validateData(fConfg, handler) {
    ld.waiting.push({ field: fConfg.name, async: handleValidation()});

    function handleValidation() {                                   /*dbug-log*///console.log('  --handleValidation field[%s] handler[%s] confg[%O] entityVal[%O]', fConfg.name, handler, fConfg, ld.validate);
        const entity = getFieldDataEntity(fConfg, ld.confg);
        return ld.validate[entity][handler](fConfg)
            .then(handleValidationResults.bind(null, fConfg));
    }
    /** Fail results: { name, value  } */
    function handleValidationResults(fConfg, results) {             /*dbug-log*///console.log('  --handleValidationResults [%s] results[%O]', fConfg.name, results);
        if (!results) { return; }
        if (results.fail) { trackFailure(results.fail.name, fConfg.value); }
        if (results.newValue) { setNewValueAndReprocessField(fConfg, results.newValue); }
        // ld.waiting.splice(ld.waiting.indexOf(fCconfg.name), 1);  //May not be necessary with the promises
    }
}
function setNewValueAndReprocessField(fConfg, newValue) {           /*dbug-log*///console.log('  -- setNewValueAndReprocessField [%s] newValue[%s] fConfg[%O]', fConfg.name, newValue, fConfg);
    fConfg.value = newValue;
    wrangleField(fConfg);
}
/* ----------------------- SET DATA ----------------------------------------- */
/** Loops through the field's prep methods to handle data-preparation. */
function handleDataPreparation(fKey, fConfg) {
    Object.keys(fConfg.prep).forEach(handleDataPrep);

    function handleDataPrep(h) {
        if (isValidationMethod(fConfg, h)) { return validateData(fConfg, h);  }
        getPrepMethod(h)(fKey, fConfg, ...fConfg.prep[h]);
    }
}
/**
 * @param  {string} h  Data-prep method-name, specified in the fields's confg.
 * @return {function}  Data-prep method
 */
function getPrepMethod(h) {
    const map = {
        buildTaxonDisplayName: buildTaxonDisplayName,
        handleAuthorNames: handleAuthorNames,
        handleSeasonTags: handleSeasonTags,
        handleSecondaryTag: handleSecondaryTag,
        renameField: renameField,
        setCitationPages: setCitationPages,
        setCitationTitle: setCitationTitle,
        setContributors: setContributors,
        setCoreAndDetail: setCoreAndDetail,
        setCoreData: setCoreData,
        setCoreType: setCoreType,
        setDetailData: setDetailData,
        setDetailEntity: setDetailEntity,
        setElevationRange: setElevationRange,
        setGeoJsonData: setGeoJsonData,
        setParent: setParent,
        setSuffix: setSuffix,
        validateTags: validateTags
    };
    return map[h];
}
/**
 * Sets formatted field/property values in the server-data object.
 * @param {str} g   Data type: rel(ation) or flat
 * @param {str} p   Entity property
 * @param {msc} v   Data value
 * @param {str} k   Entity type: core or detail
 */
function setServerData(g, p, v, k = 'core') {
    if (!ld.data[k]) { initDetailDataObj(); }
    if (!v && ld.confg.action !== 'edit') { return; }               /*dbug-log*///console.log('           --setServerData [%s][%s][%s] = [%O]', k, g, p, v);
    ld.data[k][g][p] = v;
}
/* ___________________________________ GENERAL ______________________________ */
function setCoreData(g, fConfg) {                                   /*dbug-log*///console.log('               --setCoreData [%s] fConfg[%O]', g, fConfg);
    const val = getFieldValue(fConfg);
    setServerData(g, fConfg.name, val); //Value
}
function setDetailData(g, fConfg) {                                 /*dbug-log*///console.log('               --setDetailData [%s] fConfg[%O]', g, fConfg);
    const val = getFieldValue(fConfg);
    setServerData(g, fConfg.name, val, 'detail'); //Value
}
/* ___________________________________ SPECIFIC _____________________________ */
function renameField(g, fConfg, name, dKey = 'core') {              /*dbug-log*///console.log('               --renameField [%s]entity[%s] fConfg[%O]', name, dKey, g, fConfg);
    setServerData(g, name, getFieldValue(fConfg), dKey);
}
function setCoreType(g, fConfg) {                                   /*dbug-log*///console.log('               --setCoreType [%s] fConfg[%O]', g, fConfg);
    if (typeof fConfg.value !== 'string') { return trackFailure(fConfg.name, fConfg.value); }
    setServerData(g, fConfg.entity, fConfg.value);  //String type name
}
function setParent(g, fConfg, entity) {                             /*dbug-log*///console.log('               --setParent [%s]entity[%s] fConfg[%O]', g, entity, fConfg);
    const prop = 'Parent' + entity;
    const val = getFieldValue(fConfg);
    if (isNaN(val)) { return trackFailure(prop, val); }
    setServerData(g, prop, val);
}
function setDetailEntity(g, fConfg) {                               /*dbug-log*///console.log('               --setDetailEntity [%s] fConfg[%O]', g, fConfg);
    const val = getFieldValue(fConfg);
    if (isNaN(val)) { return trackFailure(fConfg.name, val); }
    setServerData(g, fConfg.name, val, 'detail'); //Value
}
function setCoreAndDetail(g, fConfg, emptyString) {
    ['core', 'detail'].forEach(e => setServerData(g, fConfg.name, fConfg.value, e));
}
/* ______________________________________ ENTITY ____________________________ */
    /* ----------------------- AUTHOR --------------------------------------- */
/** Handles Author names */
function handleAuthorNames(g, fConfg) {
    const names = getAuthNameValues(ld.confg.fields);
    setServerData('flat', 'DisplayName', buildAuthDisplayName(names));
    setServerData('flat', 'DisplayName', buildAuthDisplayName(names), 'detail');
    setServerData('flat', 'FullName', buildAuthFullName(names), 'detail');
}
function getAuthNameValues(fields) {                                /*dbug-log*///console.log('--getAuthNameValues fields[%O]', fields);
    const sufx = fields.Suffix.value;
    return {
        first: fields.FirstName.value,
        middle: fields.MiddleName.value,
        last: fields.LastName.value,
        suffix: sufx && sufx[sufx.length-1] !== '.' ? sufx+'.' : sufx
    };
}
function buildAuthDisplayName(names) {                              /*dbug-log*///console.log('--buildAuthDisplayName names[%O]', names);
    if (!names.first) { return names.last; }
    const name = [names.last+',', names.first, names.middle, names.suffix];
    return name.filter(n => n).join(' ');
}
function buildAuthFullName(names) {                                 /*dbug-log*///console.log('--buildAuthFullName names[%O]', names);
    return Object.values(names).filter(n => n).join(' ');
}
/** Ensure value saved without punctuation */
function setSuffix(g, fConfg) {
    const v = fConfg.value;
    const sufx = v && v[v.length-1] == '.' ? v.slice(0, -1) : v;
    setServerData('flat', 'Suffix', sufx, 'detail');
}
/** [setContributors description] */
function setContributors(g, fConfg) {                               /*dbug-log*///console.log('           --setContributors fConfg[%O]', fConfg);
    const auths = ld.confg.fields.Author.value;
    const data = Object.keys(auths).length ? getContribs(fConfg.value) :
        getContribs(ld.confg.fields.Editor.value, true);
    setServerData(g, 'Contributor', data);
}
function getContribs(vals, isEditor = false) {                      /*dbug-log*///console.log('           --getContributorData editors?[%s] vals[%O]', isEditor, vals);
    const data = {};
    Object.keys(vals).forEach(buildContributorData);
    return data;

    function buildContributorData(ord) {
        const contrib = {
            ord: ord,
            isEditor: isEditor
        };
        ifQuarantinedFlag(vals[ord], contrib);                      /*dbug-log*///console.log('               --buildContributorData [%O]', contrib);
        data[vals[ord]] = contrib;
    }
}
function ifQuarantinedFlag(id, contrib) {
    const isQuarantined = checkIfQuarantinedEntity(id, 'source');
    if (!isQuarantined) { return; }
    contrib.quarantined = true;
}
    /* ----------- CITATION ------------------------------------------------- */
/** [setCitationTitle description] */
function setCitationTitle(g, fConfg) {
    // const title = _u('stripString', [fConfg.value]);
    const title = stripString(fConfg.value);
    setServerData('flat', 'Title', title, 'detail');
    setServerData('flat', 'DisplayName', getUniqueCitationName(title), 'detail');
    setServerData('flat', 'DisplayName', getUniqueCitationName(title));
}
/**
 * Removes white space at beginning and end, and any ending period.
 * TEMP: BUG WITH ENCORE WEBPACK PRODUCTION WHERE "_u" IS UNDEFINED ABOVE, BUT
 * NOT IN DEVELOPMENT MODE OR IN OTHER AREAS OF THIS CODE. EVENTUALLY PASTED THE
 * UTIL METHOD HERE TO BE ABLE TO MOVE FORWARD...
 */
function stripString(text) {
    const str = text.trim();
    return str.charAt(str.length-1) === '.' ? str.slice(0, -1) : str;
}
function getUniqueCitationName(citationTitle) {
    const pubTitle = ld.confg.fields.ParentSource.misc.pub.displayName;/*dbug-log*///console.log('--getUniqueCitationName citationTitle[%s] pubTitle[%s]', citationTitle, pubTitle)
    return pubTitle == citationTitle ? citationTitle + '(citation)' : citationTitle;
}
/** Validates page range. */
function setCitationPages(g, fConfg) {
    const pieces = fConfg.value.split('-');
    if (pieces.length !== 2 || pieces[0] > pieces[1]) { return trackFailure('Pages'); }
    setServerData('flat', 'PublicationPages', fConfg.value, 'detail');
}
    /* ------------------- INTERACTION -------------------------------------- */
function getTagsValue() {
    const tags = ld.data.core.rel.Tags;
    return !tags ? [] : tags;
}
function handleSecondaryTag(g, fConfg) {
    const type = _cmbx('getSelTxt', ['Source']);
    const tags = getTagsValue();                                    /*dbug-log*///console.log('  -- handleSecondaryTag type[%s] tags[%O] fConfg[%O]', type, tags, fConfg);
    updateSourceTags(type, tags);
    setServerData('rel', 'Tags', tags);
}
function updateSourceTags(type, tags) {
    const id = getSecondaryId(_state('getEntityRcrds', ['tag']));   /*dbug-log*///console.log('--updateSourceTags id[%s]', id);
    if (type === 'Secondary') {
        tags.push(id);
    } else if (tags.indexOf(id) !== -1){
        tags.splice(tags.indexOf(id), 1);
    }
}
function  getSecondaryId(tags) {
    return Object.values(tags).find(t => t.displayName === 'Secondary').id;
}
function handleSeasonTags(g, fConfg) {                              /*dbug-log*///console.log('  -- handleSeasonTags seasons[%O]', fConfg.value);
    const tags = getTagsValue();
    tags.push(...fConfg.value);
    setServerData('rel', 'Tags', tags);
}
/** [validateTags description] */
function validateTags(g, fConfg) {
    const typeTags = fConfg.misc.typeTags;
    const val = fConfg.value ? fConfg.value : [];
    if (typeTags && typeTags.length && fConfg.required) { ensureTypeTagSelected(typeTags, fConfg.value); }
    setServerData('rel', 'Tags', val);
}
function ensureTypeTagSelected(typeTags, selected) {                /*dbug-log*///console.log('--ensureTypeTagSelected tags[%O] selected[%O]', typeTags, selected);
    if (typeTags.find(ifTypeTagSelected)) { return; }
    trackFailure('InteractionTags', selected);

    function ifTypeTagSelected(tag) {                               /*dbug-log*///console.log('--ifTypeTagSelected[%O]', tag);
        return selected.indexOf(String(tag.id)) !== -1;
    }
}
    /* ----------- LOCATION/GEOJSON ----------------------------------------- */
/** Handles detail-entity data */
function setGeoJsonData(g, fConfg) {                                /*dbug-log*///console.log('               --setGeoJsonData [%s] fConfg[%O]', g, fConfg);
    const displayPoint = getDisplayCoordinates(fConfg.value, ld.confg.fields.Longitude.value);
    setServerData('flat', 'DisplayPoint', displayPoint, 'detail');
    setServerData('flat', 'Type', 'Point', 'detail');
    setServerData('flat', 'Coordinates', getCoordValue(displayPoint), 'detail');
}
function getDisplayCoordinates(lat, lng) {
    return JSON.stringify([ lng, lat ]);
}
function getCoordValue(displayPoint) {
    const geoJson = _state('getFormState', ['top', 'geoJson']);
    return geoJson ? geoJson.coordinates : displayPoint;
}
/** Validates and sets elevation range. */
function setElevationRange(g, fConfg) {                             /*dbug-log*///console.log('               -- setElevationRange [%s] fConfg[%O]', g, fConfg);
    const elevLow = ld.confg.fields.Elevation.value;
    if (fConfg.value < elevLow) { return trackFailure('Elevation'); }
    setServerData('flat', fConfg.name, fConfg.value);
}
    /* ----------------------- TAXON ---------------------------------------- */
function buildTaxonDisplayName(g, fConfg) {
    const rank = ld.confg.action === 'create' ?
        ld.confg.fields.Rank.value : _cmbx('getSelTxt', ['Rank']);
    const dName = rank === 'Species' ? fConfg.value : rank +' '+ fConfg.value;/*dbug-log*///console.log('--buildTaxonDisplayName rank[%s] name[%s]', rank, dName);
    setServerData('flat', 'DisplayName', dName);
}
/* ========================= QUARANTINED DATA =============================== */
function checkIfQuarantinedEntity(id, entity) {
    const rcrd = _state('getRcrd', [entity, id]);                   /*dbug-log*///onsole.log('-- checkIfQuarantinedEntity entity[%s]id[%s] = [%O]', entity, id, rcrd);
    return rcrd.quarantined;
}
/* =========================== TRACK FAILUTES =============================== */
function trackFailure(prop, value) {                                /*dbug-log*///console.log('--trackFailure prop[%s] val[%O]', prop, value);
    if (!ld.data.fails) { ld.data.fails = {}; }
    ld.data.fails[prop] = value;
}
/** Handles field-failure alerts. */
function alertIfFailures() {
    if (!ld.data.fails) { return; }
    const genErr = handleFieldErrorsAndReturnGeneralFails(ld.data.fails);
    if (!genErr.length) { return; }                                 /*perm-log*///console.log('--alertIfFailures allFails[%O] genErrs[%O]', ld.data.fails, genErr);
    _alert('alertIssue', ['dataPrepFail', JSON.stringify(genErr) ]);
}
function handleFieldErrorsAndReturnGeneralFails(fails) {
    const fLvl = ld.confg.group;
    const map = {
        Elevation: showFormFieldAlert.bind(null, 'ElevationMax', 'invalidRange', fLvl),
        Pages: showFormFieldAlert.bind(null, 'Pages', 'invalidRange', fLvl),
        InteractionTags: showFormFieldAlert.bind(null, 'InteractionTags', 'needsTypeTag', fLvl),
        Taxonym: showFormFieldAlert.bind(null, 'DisplayName', 'noNameMatch', fLvl),
    };
    const unhandled = Object.keys(ld.data.fails).map(f => map[f] ? map[f]() : null).filter(f=>f);
    if (!unhandled.length) { ld.data.fails = 'handled'; }
    return unhandled;
}
function showFormFieldAlert(field, tag, fLvl) {
    _val('showFormValAlert', [...arguments]);
    delete ld.data.fails[field];
}