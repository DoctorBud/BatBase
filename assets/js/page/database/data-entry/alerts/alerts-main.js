/**
 * Handles custom validation alerts. Forms utilize HTML5 validation where possible.
 * TODO: Refactor
 */
import * as a from './alerts.js';

export function dataPrepFailure() {
    return a.dataPrepFailure(...arguments);
}
export function showServerValAlert() {
    return a.showServerValAlert(...arguments);
}
export function formSubmitError() {
    return a.formSubmitError(...arguments);
}
export function errUpdatingData() {
    return a.errUpdatingData(...arguments);
}
export function alertFormOpen() {
    return a.alertFormOpen(...arguments);
}
export function formInitAlert() {
    return a.formInitAlert(...arguments);
}
export function showFormValAlert() {
    return a.showFormValAlert(...arguments);
}
export function clearAnyGroupAlerts() {
    return a.clearAnyGroupAlerts(...arguments);
}
export function clrContribFieldAlert() {
    return a.clrContribFieldAlert(...arguments);
}
export function clearAlert() {
    return a.clearAlert(...arguments);
}
export function clearActiveAlert() {
    return a.clearActiveAlert(...arguments);
}