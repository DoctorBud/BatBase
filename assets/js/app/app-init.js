/**
 * Initializes the interface, libraries, and features used throughout the
 * Eco-Interactions website.
 *
 * TOC
 *    STYLES AND GLOBAL JQUERY
 *    UI INIT
 *      HEADER AND MAIN NAV
 *      TOS
 *      AUTH-DEPENDENT INIT
 *    PAGE SPECIFIC
 *      DATATABLES
 *      SUBMIT PDF
 *    BROWSER SPECIFIC
 */
import { initUtil } from '~util';
import initHeaderAndNav from './header/header-main.js';
import initFeedbackUi from '../page/feedback/feedback.js';
import * as misc from './misc/app-misc-main.js';

export function initSiteCore() {                                    /*dbug-log*///console.log('initSiteCore');
    misc.initSentryIssueTracking();
    setGlobalJquery();
    initUi();
    initUtil();
}
/** Necessary for access during testing. */
function setGlobalJquery() {
    global.$ = $;
    global.jQuery = $;
}
/* ========================== UI INIT ======================================= */
function initUi() {
    requireStyles();
    initHeaderAndNav();
    misc.initTos();
    handlePageSpecificUiInit();
    authDependentInit();
    $('#b-overlay').hide(); // Hides loading overlay on mobile
    $('.popup').show();
}
function requireStyles() {
    require('styles/base/reset.styl');
    require('styles/ei.styl');
    require('styles/css/lib/introjs.min.css');
}
/* ------------------- AUTH-DEPENDENT INIT ---------------------------------- */
function authDependentInit() {
    const userRole = $('body').data("user-role");
    if (userRole === 'visitor') { return; }
    initFeedbackUi();
    if (ifUserCanEditPage(userRole)) { misc.initWysiwyg(userRole); }
}
function ifUserCanEditPage(userRole) {
    return userRole === 'admin' && window.outerWidth > 550 || userRole === 'super';
}
/* ========================== PAGE SPECIFIC ================================= */
function handlePageSpecificUiInit() {
    misc.initDataTable();
    misc.showOverlayOnMobile();
    misc.initTogglePassword();
    clearFieldForPdfSubmissions();
}
/* ------------------- SUBMIT PDF ------------------------------------------- */
/** Not quite sure how to show a success message and reload form, will loop back when there's more time. */
function clearFieldForPdfSubmissions() {
    if (window.location.pathname.includes('upload/publication')) {
        $("form[name='App_file_upload']:first-child div").css('justify-content', 'start');
        $('#App_file_upload_title, #App_file_upload_description').val(''); //Clears fields after form submit.
        $('.vich-image a').remove();
    }
}