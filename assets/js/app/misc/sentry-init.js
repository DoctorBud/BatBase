/**
 * Inits automated issue-tracking, Sentry.
 *
 * Export
 *     initSentryIssueTracking
 */
import { BrowserTracing } from "@sentry/tracing";

/* ================= SENTRY ERROR TRACKING ================================== */
export function initSentryIssueTracking() {
    if ($('body').data('env') !== 'prod') { return; }
    initSentry();
}
/* --------------------- INIT SENTRY ---------------------------------------- */
function initSentry () {
    Sentry.init({
        dsn: 'https://28ec22ce887145e9bc4b0a243b18f94f@o955163.ingest.sentry.io/5904448',
        integrations: [ new BrowserTracing() ],
        tracesSampleRate: 0.2,  // sets a uniform sample rate
        blacklistUrls: ['dev.batbase.org'],
        beforeSend(event, hint) {  /*Catches rouge 3rd-party error, likely google recaptcha  https://github.com/getsentry/sentry-javascript/issues/2514 */
            if (hint.originalException === "Timeout") return null;
            return event;
        }
    });
    configureSentryUserData($('body').data('user-name'), $('body').data('user-role'));
}
function configureSentryUserData (userName, userRole) {
    const user = { username: userName, role: userRole };            /*temp-log*/console.log('--Sentry user [%O]', user);
    Sentry.configureScope(scope => {
        scope.setUser(user);
    })
}