/**
 * Entry-point for miscellaneous site init-methods.
 */
import * as block from './mobile-block.js';
import * as table from './data-tables.js';
import * as sentry from './sentry-init.js';
import * as toggle from './toggle-password-visiblity.js';
import * as tos from './tos.js';
import * as wysiwyg from './wysiwyg.js';

export function showOverlayOnMobile() {
    block.showOverlayOnMobile();
}
export function initSentryIssueTracking() {
    sentry.initSentryIssueTracking();
}
export function initDataTable() {
    table.initDataTable();
}
export function initTogglePassword() {
    toggle.initTogglePassword();
}
export function initTos() {
    tos.initTosPopup();
}
export function initWysiwyg() {
    wysiwyg.initWysiwyg();
}