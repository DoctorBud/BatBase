/**
 * Toggles visibility for password inputs inside of password-cntnr elems.
 */
export function initTogglePassword() {                      /*dbug-log*///console.log('initTogglePassword');
    if (!pageHasPasswordField(window.location.pathname)) { return; }
    addPasswordToggleFunctionality();
}
function addPasswordToggleFunctionality() {
    $('input[type="password"]').each((i, el) => setToggleListeners(el));
}
function setToggleListeners(el) {                                   /*dbug-log*///console.log('addPasswordToggleFunctionality el[%O]', el);
    const icon = el.nextElementSibling;
    $(icon).click(togglePassword.bind(null, el, icon));
}
function togglePassword(input, toggle) {                            /*dbug-log*///console.log('togglePassword input[%O] toggle[%O]', input, toggle);
    const hide = 'fa-eye-slash';
    const show = 'fa-eye';

    if ($(toggle).hasClass(hide)) {
        $(toggle).removeClass(hide).addClass(show);
        input.type = 'text';
    } else {
        $(toggle).removeClass(show).addClass(hide);
        input.type = 'password';
    }
}
function pageHasPasswordField(path) {
    const pages = ['login', 'register', 'reset-password'];
    return pages.find(p => path.includes(p));
}