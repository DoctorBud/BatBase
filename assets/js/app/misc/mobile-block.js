/**
 * For pages that are not able to be used on mobile devices, show a popup directing
 * users to view page on a computer.
 */
import { _u } from '~util';

export function showOverlayOnMobile() {
    if (_u('isAdminUser')) { return }
    const mblMsg = getMobileMsg();
    if (!mblMsg || $('body').data('env') == 'test') { return; }
    showMobilePopupMsg(mblMsg);
}
function getMobileMsg() {
    const map = { search: 1200, 'view-pdfs': 800, feedback: 800};
    const winWidth = Math.round(getPageWidth());
    const path = window.location.pathname;
    const pg = Object.keys(map).find(pg => path.includes(pg));
    if (!pg || isSearchPgOnApple(pg) || winWidth > map[pg])  { return false; }
    return getMobileMsgHtml(map[pg])

    /** Note: search page code doesn't load on mobile devices. */
    function isSearchPgOnApple(pg) {                                /*dbug-log*///console.log('pg = %s, apple? ', pg, ['Safari', 'iPhone'].indexOf($('body').data('browser')) !== -1)
        if (pg == 'search' &&
            ['Safari', 'iPhone'].indexOf($('body').data('browser')) !== -1) {
            showBrowserWarningPopup();
            return true;
        }
        function showBrowserWarningPopup() {
            const overlay = $('<div></div>').addClass('mobile-opt-overlay');
            const popup = $('<div></div>').addClass('popup');
            $(popup).html(`<center><h2>This page not supported on Safari Browser currently.</h2>`);
            $(overlay).append(popup);
            $('#detail-block').prepend(overlay);
            $('.popup').fadeIn(500);
        }
    }
    function getMobileMsgHtml(minWidth) {
        return `<center><h2>Page must be viewed on screen at least ${minWidth} pixels wide.<h2>
            <br><p>This screen is ${winWidth} pixels wide.</p></center>`;
    }
}
function getPageWidth() {
    return window.visualViewport ? window.visualViewport.width : window.innerWidth;
}
function showMobilePopupMsg(mblMsg) {
    const overlay = $('<div></div>').addClass('mobile-opt-overlay');
    const popup = $('<div></div>').addClass('popup');
    $(popup).html(mblMsg);
    $(overlay).append(popup);
    $('#detail-block').prepend(overlay);
    $('#b-overlay-popup').fadeIn(500);
}