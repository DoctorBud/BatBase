/**
 * Builds form-field inputs.
 * TODO: DOCUMENT
 *
 * Export
 *     getFieldInput
 *     buildMultiSelectField
 *
 * TOC
 *     INPUT BUILDERS
 *         INPUT
 *         TEXTAREA
 *     FINISH INPUT BUILD
 *         CHANGE HANDLER
 *         REQUIRED FIELDS
 */
import { _el, _opts, _u } from '~util';
import { handleInputValidation } from './val-input.js';

/* ======================= INPUT BUIDLERS =================================== */
export function getFieldInput(fConfg) {                             /*dbug-log*///console.log('+--getFieldInput [%O]', fConfg);
    return Promise.resolve(getInput(fConfg))
        .then(input => setFieldValue(fConfg, input))
        .then(input => handleInputValidation(fConfg.type, input))
        .then(input => finishInputBuild(fConfg, input));  //returns fConfg with the input elem added.
}
function getInput(f) {
    return {
        checkbox:       buildCheckbox,
        dateRange:      buildDateRangeInputs, //requires ID in config (Inputs will have ID's appended with '-start' and '-end')
        doi:            buildInput,
        fullTextArea:   buildLongTextArea,
        lat:            buildInput,
        lng:            buildInput,
        multiSelect:    buildMultiSelectFieldCntnr,
        num:            buildNumberInput,
        numRange:       buildNumberRangeInputs,  //requires ID in config (Inputs will have ID's appended with '-start' and '-end')
        page:           buildInput,
        select:         buildSelect,
        tags:           buildSelect,
        text:           buildInput,
        textArea:       buildTextArea,
        url:            buildUrlInput,
        year:           buildNumberInput
    }[f.type](f);
}
/* ------------------------------- INPUT ------------------------------------ */
function buildInput(f, type = 'text') {
    const attr = { type: type, class: f.class };
    const input = _el('getElem', ['input', attr]);
    return input;
}
function buildNumberInput(f) {
    return buildInput(f, 'number');
}
function buildNumberRangeInputs(f) {
    const start = buildInput(f, 'number');
    start.id = f.id + '-start';
    const end = buildInput(f, 'number');
    end.id = f.id + '-end';
    return [start, end];
}
function buildUrlInput(f) {
    return buildInput(f, 'url');
}
function buildCheckbox(f) {
    return buildInput(f, 'checkbox');
}
/* ----------------------------- TEXTAREA ----------------------------------- */
function buildTextArea(f) {
    return _el('getElem', ['textarea', {class: f.class }]);
}
function buildLongTextArea(f) {
    const attr = { class: f.class, id:'txt-'+f.id };
    return _el('getElem', ['textarea', attr]);
}
/* --------------------- SINGLE SELECT/COMBOS ------------------------------- */
/**
 * Creates and returns a select dropdown for the passed field. If it is one of
 * a larger set of select elems, the current count is appended to the id. Adds
 * the select's fieldName to the subForm config's 'selElem' array to later
 * init the 'selectize' combobox.
 */
function buildSelect(f) {                                           /*dbug-log*///console.log("       --buildSelect [%O]", f);
    const optStrings = f.misc ? f.misc.opts : null;
    return _opts('getFieldOptions', [f.name, optStrings])
        .then(finishSelectBuild.bind(null, f));
}
function finishSelectBuild(f, opts) {                               /*dbug-log*///console.log('           --finishSelectBuild fConfg[%O] opts[%O]', f, opts);
    const attr = { class: f.class, id: 'sel-' + f.id };
    f.combo = true; //Flag for the combobox selectize-library
    return _el('getSelect', [opts, attr]);
}
/* ---------------------- DATE-RANGE SELECT --------------------------------- */
function buildDateRangeInputs(f) {                                  /*dbug-log*///console.log("--buildDateRangeInput field[%O]", f);
    const start = buildInput(f);
    start.id = f.id + '-start';
    const end = buildInput(f);
    end.id = f.id + '-end';
    if (f.value) { $(start).data('init-val', f.value); }
    return [start, end];
}
/* ---------------------- MULTI-SELECT/COMBOS ------------------------------- */
/**
 * Creates a select dropdown field wrapped in a div container that will
 * be reaplced inline upon selection. Either with an existing Author's name,
 * or the Author create form when the user enters a new Author's name.
 */
function buildMultiSelectFieldCntnr(f) {                             /*dbug-log*///console.log("       --buildMultiSelectFieldCntnr fConfg[%O]", f);
    return buildMultiSelectField(f)
        .then(buildMultiSelectCntnr.bind(null, f));
}
/**
 * Builds an input inside of a container, allowing subsequent inputs to be added.
 */
export function buildMultiSelectField(f) {                          /*dbug-log*///console.log("--buildMultiSelectField [%O]", f);
    return buildSelect(f)
        .then(finishFieldInput.bind(null, f));
}
function finishFieldInput(f, input) {                               /*dbug-log*///console.log('--finishFieldInput fConfg[%O] input[%O]', _u('snapshot', [f]), input);
    const confg = getMultiInputFieldConfg(f, input);
    input.id += f.count;
    return _el('getFieldElems', [confg]);
}
function getMultiInputFieldConfg(f, input) {
    return {
        'class': f.class,
        group: f.group,
        id: f.name + f.count,
        input: input,
        label: getCntLabel(f.count)+' '+f.name,
        name: f.name+f.count,
        required: f.required || false,
        type: 'select'
    };
}
function getCntLabel(cnt) {
    const map = { 1: '1st', 2:'2nd', 3:'3rd' };
    return cnt in map ? map[cnt] : cnt+'th';
}
function buildMultiSelectCntnr(f, field) {
    f.input = field;
    return _el('getFieldElems', [f]);
}
/* --------------------------- SET VALUE ------------------------------------ */
function setFieldValue(f, input) {                                  /*dbug-log*///console.log('--setFieldValue [%s][%O]', f.name, _u('snapshot', [f]))
    if (f.type === 'multiSelect') {
        handleMultiFieldInitVal(f);
    } else {
        setInputVal(f.value, input);
    }
    return input;
}
function setInputVal(fVal, input) {
    if (!fVal) { return; }
    const val = _u('isObj', fVal) ? fVal.value : fVal;
    $(input).val(val);
}
function handleMultiFieldInitVal(f) {
    if (f.value && _u('isObj', [f.value])) { return; }
    f.value = {};
}
/* ========================== FINISH BUILD ================================== */
function finishInputBuild(f, input) {                               /*dbug-log*///console.log('   --finishInputBuild f[%O] input[%O]', f, input);
    f.input = input;
    return f;
}