/**
 * Utility elem-methods.
 *
 * TOC
 *     BUILD
 *     MISC
 *         TOGGLE EXPAND
 */
import * as build from './build/elem-build-main.js';
import * as misc from './misc/elem-misc-main.js';

/* ======================= BUILD ============================================ */
export function getElem() {
    return build.getElem(...arguments);
}
export function getSelect() {
    return build.getSelect(...arguments);
}
export function getFormFooter() {
    return build.getFormFooter(...arguments);
}
export function getFieldElems() {
    return build.getFieldElems(...arguments);
}
export function getFieldInput() {
    return build.getFieldInput(...arguments);
}
export function buildMultiSelectField() {
    return build.buildMultiSelectField(...arguments);
}
/* ======================== MISC ============================================ */
/* -------------------- TOGGLE EXPAND --------------------------------------- */
export function setExpandableContentListeners() {
    misc.setExpandableContentListeners(...arguments);
}
export function setElemExpandableListeners() {
    misc.setElemExpandableListeners(...arguments);
}
export function toggleContent() {
    misc.toggleContent(...arguments);
}