/**
 *
 *
 * EXPORT
 *
 * TOC
 *     TOGGLE EXPAND/COLLAPSE
 *
 */
import * as expandable from './expandable-content.js';
/* ==================== TOGGLE EXPAND/COLLAPSE ============================== */

export function setExpandableContentListeners() {
    expandable.setExpandableContentListeners();
}
export function setElemExpandableListeners() {
    expandable.setElemExpandableListeners(...arguments);
}
export function toggleContent() {
    expandable.toggleContent(...arguments);
}