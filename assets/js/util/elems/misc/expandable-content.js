/**
 *
 *
 * EXPORT
 *     setExpandableContentListeners
 *     setElemExpandableListeners
 *
 * TOC
 *     TOGGLE EXPAND/COLLAPSE
 *
 */

/**
 * <DIV.expandable>
 *     <H#.toggle-trigger>
 *     H#.toggle-trigger.next()
 */
/* ====================== TOGGLE LISTENER =================================== */
export function setExpandableContentListeners() {                   /*dbug-log*///console.log('-- setExpandableContentListeners')
    $(".toggle-trigger").click(toggleExpandableContent);
}
export function setElemExpandableListeners($elem) {                 /*dbug-log*///console.log('-- setElemExpandableListeners $elem[%O]', $elem)
    $elem.find(".toggle-trigger").click(toggleExpandableContent);
}
/* ==================== TOGGLE EXPAND/COLLAPSE ============================== */
function toggleExpandableContent() {
    const $header = $(this);
    const $content = $header.next(); //getting the next element
    const toggleTo = $content.hasClass('closed') ? 'expanded' : 'collapsed';/*dbug-log*///console.log('-- toggleExpandableContent to [%s]', toggleTo);
    toggleElemContent($header, $content, toggleTo);
    updateContentHeader($header, toggleTo);
}
function toggleElemContent($header, $content, toggleTo) {           /*dbug-log*///console.log('-- toggleContent to[%s] [%O]', toggleTo, $content);
    if (toggleTo === 'expanded') { expandContent($header, $content); }
    else { collapseContent($header, $content); }
}
export function toggleContent(selector, toggleKey = null) {
    const $header = $(`${selector}.toggle-trigger`);                /*dbug-log*///console.log('-- toggleContent header[%s][%O]', `${selector} .toggle-trigger`, $header);
    const toggleTo = toggleKey ? toggleKey : getNextToggleState($header);
    toggleElemContent($header, $header.next(), toggleTo);
}
function getNextToggleState($elem) {
    return $elem.hasClass('closed') ? 'expanded' : 'collapsed';
}
/* ---------------------- EXPAND CONTENT ------------------------------------ */
function expandContent($header, $content) {                         /*dbug-log*///console.log('-- expandContent [%O]', $content);
    $header.removeClass('closed');
    $content.removeClass('closed');
}
/* --------------------- COLLAPSE CONTENT ----------------------------------- */
function collapseContent($header, $content) {                       /*dbug-log*///console.log('-- collapseContent [%O]', $content);
    $header.addClass('closed');
    $content.addClass('closed');
}
/* --------------------- UPDATE HEADER TEXT --------------------------------- */
function updateContentHeader($header, toggleTo) {
    let newTxt = $header.data(toggleTo);
    if (newTxt === false) { return; }
    if (!newTxt) { newTxt = toggleTo == 'expanded' ? 'Collapse' : 'Expand'; }
    $header.text(newTxt);
}