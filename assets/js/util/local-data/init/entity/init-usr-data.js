/**
 * Modifies and sorts user-data for local storage.
 * - savedFilters - an object with each filter's id (k) and data (v)
 * - savedFiltersNames - an object with each filter's name(k) and { id, focus-view }
 * - dataLists - an object with each list's id (k) and data (v)
 * - dataListNames - an object with each list's name (k) and id (v).
 * - user - current user's name or 'visitor'
 *
 * - editorNames - an object with each editor's name(k) and id(v)
 *
 * Export
 *     getFilterListGroupString
 *     modifyCustomUsrDataForLocalDb
 *     modifyUsrDataForLocalDb
 *
 * TOC
 *     CUSTOM USER LIST AND FILTER DATA
 *     USER ENTITY
 */
import * as db from '../../local-data-main.js';
import { getNameObj } from '../init-helpers.js';
/* ====================== CUSTOM USER LIST AND FILTER DATA ================== */
export function modifyCustomUsrDataForLocalDb(data) {               /*dbug-log*///console.log("modifyCustomUsrDataForLocalDb called. data = %O", data);
    const filters = {};
    const filterIds = [];
    const int_sets = {};
    const int_setIds = [];

    data.lists.forEach(addToDataObjs);
    db.setDataInMemory('savedFilters', filters);
    db.setDataInMemory('savedFilterNames', getFilterOptionGroupObj(filterIds, filters));
    db.setDataInMemory('dataLists', int_sets);
    db.setDataInMemory('dataListNames', getNameObj(int_setIds, int_sets));
    db.setDataInMemory('user', getUserName());
    db.deleteMmryData('list');

    function addToDataObjs(l) {
        const entities = l.type == 'filter' ? filters : int_sets;
        const idAry = l.type == 'filter' ? filterIds : int_setIds;
        entities[l.id] = l;
        idAry.push(l.id);
    }
}
function getUserName() {
    return $('body').data('user-name') ? $('body').data('user-name') : 'visitor';
}
function getFilterOptionGroupObj(ids, filters) {                    /*dbug-log*///console.log('getFilterOptionGroupObj ids = %O, filters = %O', ids, filters);
    const data = {};
    ids.forEach(buildOptObj);
    return data;

    function buildOptObj(id) {
        return data[filters[id].displayName] = {
            value: id, group: getFilterListGroupString(filters[id])
        }
    }
}
export function getFilterListGroupString(list) {
    list.details = JSON.parse(list.details);
    const map = {
        srcs: 'Source',
        locs: 'Location',
        taxa: 'Taxon',
    };
    const view = list.details.view ? list.details.view.text : false;
    const focus = map[list.details.focus];                          /*dbug-log*///console.log('getFilterListGroupString. list[%O]', list)
    return !view ? focus : `${focus} - ${view}`;
}
/* =========================== USER ENTITY ================================== */
export function modifyUsrDataForLocalDb(data) {                     /*dbug-log*///console.log("modifyUsrDataForLocalDb called. data = %O", data);
    db.setDataInMemory('editorNames', getEditors(data.users));
}
function getEditors(users) {
    const editorIds = Object.keys(users).filter(isEditor);
    const data = {};
    editorIds.forEach(id => data[users[id].fullName] = id);         /*dbug-log*///console.log("nameDataObj = %O", data);
    return data;

    function isEditor(id) {
        return users[id].roles.indexOf('ROLE_EDITOR') !== -1;
    }
}