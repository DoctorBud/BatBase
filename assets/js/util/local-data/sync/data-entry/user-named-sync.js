/**
 * Updates local data after a user-named list is updated, interaction lists or
 * filter sets.
 *
 * Export
 *     updateUserNamedList
 */
import * as db from '../../local-data-main.js';

export function updateUserNamedList(data, action) {                 /*perm-log*/console.log('   --Updating [%s] stored list data[%O]', action, data);
    let rcrds, names;
    const list = action == 'delete' ? data : JSON.parse(data.entity);
    const rcrdKey = list.type == 'filter' ? 'savedFilters' : 'dataLists';
    const nameKey = list.type == 'filter' ? 'savedFilterNames' : 'dataListNames';

    return db.getData([rcrdKey, nameKey])
        .then(storedData => syncListData(storedData))
        .then(trackTimeUpdated.bind(null, 'UserNamed', list));

    function syncListData(storedData) {                             /*dbug-log*///console.log('syncListData rcrds[%s] names[%s] = [%O]', rcrdKey, nameKey, storedData);
        rcrds = storedData[rcrdKey];
        names = storedData[nameKey];

        if (action == 'delete') { removeListData();
        } else { updateListData(); }

        db.setData(rcrdKey, rcrds);
        db.setData(nameKey, names);
    }
    function removeListData() {
        delete rcrds[list.id];
        delete names[list.displayName];
    }
    function updateListData() {
        rcrds[list.id] = list;
        const listData = list.type !== 'filter' ? list.id :
            {value: list.id, group: db.getFilterListGroupString(list)};
        names[list.displayName] = listData;                         /*dbug-log*///console.log('--updateListData [%O]', listData);
        if (data.edits && data.edits.displayName) { delete names[data.edits.displayName.old]; }
    }
}

function trackTimeUpdated(entity, rcrd) {
    db.getData('lclDataUpdtdAt').then(stateObj => {
        stateObj[entity] = rcrd.serverUpdatedAt;
        return db.setData('lclDataUpdtdAt', stateObj);
    });
    return Promise.resolve()
}