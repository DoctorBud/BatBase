/**
 * Entity-data pending approval are quarantined locally.
 *     - Quarantined IDs are randomly altered to ensure unique values
 *     - Relationships with quarantined-entities are set in the serialized
 *         entity-data returned from the server.
 *
 * Export
 *     handleQuarantinedData
 *
 * TOC
 *     MODIFY FOR QUARANTINED STORAGE
 *     SET QUARANTINED RELATIONSHIPS
 */
import { _u } from '~util';
/**
 * Entity-data pending approval are quarantined locally.
 *     - Quarantined IDs are randomly altered to ensure unique values
 *     - Relationships with quarantined-entities are set in the serialized
 *         entity-data returned from the server.
 *
 * @param  {obj} data
 * @param  {str} data.core          Entity classname
 * @param  {obj} data.coreEntity    JSON Serialized record
 * @param  {int} data.coreId        Record ID
 * @param  {str} data.detail        Entity classname
 * @param  {obj} data.detailEntity  JSON Serialized record
 * @param  {int} data.detailId      Record ID
 * @param  {str} data.name          Entity display-name
 *
 * @param  {obj} fConfg             Data-Entry form configuration
 */
/** TODO: WITH QUARANTINED EDITS, USE MODIFIED AND HIDE ORIGINAL. */
export default function handleQuarantinedData(data, fConfg) {       /*dbug-log*///console.log('--handleQuarantinedData data[%O] fConfg[%O]', data, fConfg);
    handleQuarantineEntity(data.coreEntity);
    handleLocallyQuarantinedRelationships(data.coreEntity, fConfg.fields);
    if (!data.detailEntity) { return; }
    handleQuarantineEntity(data.detailEntity);
    data.coreEntity[_u('lcfirst', [data.detail])] = data.detailEntity.id;
}
/* ================== MODIFY FOR QUARANTINED STORAGE ======================== */
function handleQuarantineEntity(entity) {
    const newId = entity.id + _u('getRandomInt', [222222, 9999999]);
    entity.id = newId;
    entity.quarantined = true;
}
/* ================== SET QUARANTINED RELATIONSHIPS =========================== */
function handleLocallyQuarantinedRelationships(coreEntity, fields) {
    setQuarantinedParent(coreEntity, fields);
}
function setQuarantinedParent(coreEntity, fields) {
    if (coreEntity.parent) { return; }
    const pField = Object.values(fields).find(f => f.name.includes('Parent'));
    coreEntity.parent = pField.value;
}
