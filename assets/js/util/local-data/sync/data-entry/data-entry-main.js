/**
 * On form submit success, the new and edited data is updated in the local database.
 */
import * as list from './user-named-sync.js';
import * as sync_data from './data-entry-sync.js';
/* ===================== ENTITY UPDATES ===================================== */
/* ----------------------- USER-NAMED LIST ---------------------------------- */
export function updateUserNamedList() {
    return list.updateUserNamedList(...arguments);
}
/* ============= UPDATE DATA AFTER FORM SUBMIT ============================== */
export function afterDataEntrySyncLocalDatabase() {
    return sync_data.afterDataEntrySyncLocalDatabase(...arguments);
}
export function updateLocalEntityData() {
    return sync_data.updateLocalEntityData(...arguments);
}