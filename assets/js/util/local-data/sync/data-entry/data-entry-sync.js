/**
 * On form submit success, the new and edited data is updated in the local database.
 *
 * Export
 *     afterDataEntrySyncLocalDatabase
 *     updateLocalEntityData
 *
 * TOC
 *     UPDATE DATA AFTER FORM SUBMIT
 *         PARSE SERVER JSON
 *     UPDATE ENTITY DATA
 *         COMPLETE UPDATE
 */
import { _db } from '~util';
import * as sync from '../db-sync-main.js';
import ifSourceDataEditedUpdatedCitations from './citation-sync.js';
import handleQuarantinedData from './quarantined-data.js';

/* ============= UPDATE DATA AFTER FORM SUBMIT ============================== */
/**
 * After data-entry (create, edit, quarantine), data is added to Local Storage.
 *
 * @param  {obj} data
 * @param  {str} data.core          Entity classname
 * @param  {obj} data.coreEntity    JSON Serialized record
 * @param  {int} data.coreId        Record ID
 * @param  {str} data.detail        Entity classname
 * @param  {obj} data.detailEntity  JSON Serialized record
 * @param  {int} data.detailId      Record ID
 * @param  {str} data.name          Entity display-name
 *
 * @return {obj} data
 *         {obj} data.fails         Sync-failures object added.
 */
export function afterDataEntrySyncLocalDatabase(data, fConfg) {     /*perm-log*/console.log("   /--afterDataEntrySyncLocalDatabase data recieved[%O] fConfg[%O]", data, fConfg);
    return _db('getAllStoredData')
        .then(storeMmryAndUpdate);

    function storeMmryAndUpdate(mmry) {
        _db('setMmryDataObj', [mmry]);
        return updateLocalEntityData(data, fConfg)
            .then(() => handleFailuresAndReturnData(data));
    }
}
export function updateLocalEntityData(data, fConfg) {
    parseEntityData(data);
    if (data.quarantined) { handleQuarantinedData(data, fConfg); }
    trackEditsToLocalProps(data);
    return updateEntityData(data)
        .then(() => _db('setUpdatedDataInLocalDb'));
}
/* ------------------ PARSE SERVER JSON ------------------------------------- */
function parseEntityData(data) {
    for (let prop in data) {                                        /*dbug-log*///console.log('-- parseEntityData prop[%s] data[%O]', prop, data);
        try {
            data[prop] = JSON.parse(data[prop]);
        } catch (e) { /* Fails on string values */
            //console.log('error');
        }
    }
}
/* ------------------ TRACK LOCAL-PROP EDITS -------------------------------- */
function trackEditsToLocalProps(data) {                             /*dbug-log*///console.log('   -- trackEditsToLocalProps data[%O]', data);
    if (!data.coreEdits) { return; }
    trackLocalEdits(data, data.coreEdits);
    // trackLocalEdits(data, data.detailEdits); //Not used yet.
}
function trackLocalEdits(data, edits) {
    const map = {
        ParentTaxon: ifTaxonGroupChanged
    };
    Object.keys(edits).forEach(trackEditsToRelatedData);

    function trackEditsToRelatedData(prop) {
        return map[prop] ? map[prop](data, edits, edits[prop]) : null;
    }
}
function ifTaxonGroupChanged(data, edits, pTxnId) {
    const taxa = _db('getMmryData', ['taxon']);
    const newPrnt = taxa[pTxnId.new];
    const oldPrnt = taxa[pTxnId.old];                               /*dbug-log*///console.log('   -- ifTaxonGroupChanged newPrnt[%O] old[%O]', newPrnt, oldPrnt);
    trackEditsToData('Group', newPrnt.group.displayName, oldPrnt.group.displayName, edits);
    trackEditsToData('SubGroup', newPrnt.group.subGroup.name, oldPrnt.group.subGroup.name, edits);
}
function trackEditsToData(field, newVal, oldVal, edits) {
    if (newVal === oldVal) { return; }
    edits[field] = { new: newVal, old: oldVal };
}
/* =================== UPDATE ENTITY DATA =================================== */
function updateEntityData(data) {                                   /*dbug-log*///console.log('updateEntityData[%O]', data);
    if (ifDeletedInteraction(data)) { return Promise.resolve(); }
    sync.addCoreEntityData(data.core, data.coreEntity);
    updateDetailEntityData(data)
    sync.removeInvalidatedData(data)
    sync.retryFailedUpdates();
    return ifSourceDataEditedUpdatedCitations(data);
}
function ifDeletedInteraction(data) {
    return data.entity === '' && data.coreEntity.note.includes('[INTERACTION DELETED]');
}
function updateDetailEntityData(data) {
    if (!data.detailEntity) { return; }
    return sync.addDetailEntityData(data.detail, data.detailEntity);
}
/* --------------------- COMPLETE UPDATE ------------------------------------ */
function handleFailuresAndReturnData(data) {
    sync.reportDataSyncFailures(data)
    _db('clearTempMemory');
    return data;
}