##Tip: Syntax highlight file with YAML to see highlighting##
#
# Contents:
#   CURRENT - LOGS, QUESTIONS, TASKS, DATA CLEANUP, MEETING NOTES
#   DEV INTERNAL TASKS - Todos, refactoring, bugs
### __________________________ CURRENT _____________________________________ ###

SINCE LAST SERVER UPDATE:
    - FILTER
        - When in Taxon -> Group view, if the group has both subject and object
            interactions, a combo is added to filter the data table by interaction-role.
        - Added to all vliews, filter to any/some/all taxon-group data
    - TABLE
        - Hiding entities with '[DELETE]' from the DATABASE TABLE, bibliography, and CSV downloads
        - Merged interactions from [Location-HabType] groups into '[Location-Unspecified]'
        - Added taxon-group icons to interaction-row tree-cells.
           - Tool tips on each icon allow for quick taxon-group identification
           - Note- there's a bug that prevents the bat icon from showing in some location interactions. Not sure why.
    - FORMS
        - Configuration
            - all form-field configuration centralized
            - ~80% of the code needed for edit forms is no longer needed!
                - This greatly reflexts the general increase of flexibility when
                    reusing the forms with differnt configuration
            - Views
                - 'simple' and 'all views'
                - Taxon has a 'create' and 'edit' view
                - Fields can be stacked in the same row (eg, doi and website)
                - arbitrary number of fields in row
                    - styled by percentage based on total "fields" in row and field size (eg, year is a "small" field)
                        - empty "spacer" fields easy as well
            - Form Validation
                - Form validation has improved with each field's validation able to
                    be configured directly. Various errors are prevented by sensible
                    UI presentation, when data is entered into a field, and before
                    the form data is submitted to the server - showing helpful alerts
                    as needed. (Full documentation to come.)
                - HTML form-validation alerts added to fields that require edits due to invalid data previously entered and now automatically prevented - eg, an invalid URL or coordinate format.
        - Entity Forms
            - Generally reordered fields to what seems like a more intuitive flow for
                editors, prioritizing required fields and resizing where appropriate
            - Publication
                - Author|editor
                    - note changed to "Note: This publication type can have either authors OR editors."
                    - opposite type disabled and label hidden after selection
                    - labels now show order: 1st, 2nd, etc.
                    - reordered/resized form-fields
                    - when a blank exists in the author order, the final empty field is removed
                        to show more intuitevly the blank must be filled before continuing
                    - Duplicate authors are now prevented by removing selected authors from other combos
            - Citation
                - Data from publication added to / removed from form automatically,
                    with fields disabled/enabled to increase clarity and reduce errors.
            - Location
                - When data entered matches region/country, optionally habitat type, an alert is show to prompt editor to select existing Location-Habitat entity.
                - Territories are identified and they are selected as Country
                    - Also added to the possesing country's combo and map display's to prevent duplicates
            - Interaction
                - Subject
                    - Only groups with valid subject roles are added
                - Object
                    - auto select bats if anything other than bats are selected as the subject
                - Type
                    - Once both subject and Object have been selected, the combo is
                    filled with valid types for the taxon groups selected.
                - Tags
                    - Once type is selected, valid tags are added to the combo. If
                    there is a single required tag, it is autoselected. The field
                    is required based on the Valid Interaction selected.
                - Misc
                    - Removed pre-submit modal with subj interaction-type obj. With the
                    automation of the types, this no longer seems necessary or helpful
            - Taxon edit-form rebuilt


    - Todos
        - FORMS
            - EDIT: Reorder fields to use the space better




SINCE LAST USER UPDATE:
    - Publication upload
    - Site designed for mobile use
    - Ability to save set of filters and lists of interactions
    - Added new realms: Bacteria, Virus, Fungi, Bird, Reptile, and Amphibian.
    - Added Interaction and Taxon pages
    - Data Entry
        - Error reporting feature
        - Forms improved
            - Added mapping to location forms
            - Tutorials added to forms

    Upcoming:
        - Data review features

### ____________________________ PAST ______________________________________ ###

