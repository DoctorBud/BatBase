<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Admin controller.
 *
 * @Route("/super")
 */
class SuperAdminController extends AbstractController
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Lists all users active online in the last 24 hours.
     *
     * @Route("/online-users", name="super_user_online")
     */
    public function onlineAction()
    {
        $entities = $this->em->getRepository('App:User')->findOnlineNow();

        return $this->render('Admin/super/online.html.twig', array(
            'entities' => $entities,
        ));
    }
}