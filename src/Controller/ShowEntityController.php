<?php

namespace App\Controller;

use App\Service\LogError;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Handles individual entity (interaction and taxon) show pages.
 *
 * @Route("/")
 */
class ShowEntityController extends AbstractController
{

    private $em;
    private $logger;
    private $serializer;

    public function __construct(
        EntityManagerInterface $em,
        LogError $logger,
        SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }
/* --------------- SERIALIZE --------------- */
    private function serializeEntity($entity)
    {
        try {
            return $this->serializer->serialize($entity, 'json',
                SerializationContext::create()->setGroups(array('flattened')));
        } catch (\Throwable $e) {
            $this->logger->logError($e);
        } catch (\Exception $e) {
            $this->logger->logError($e);
        }
    }
/* ----------------------------- INTERACTION -------------------------------- */
    /**
     * Opens the Interaction show page.
     *
     * @Route("interaction/{id}", name="app_interaction_show")
     */
    public function showInteractionAction($id)
    {
        $interaction = $this->getEntity('Interaction', $id);
        if (!$interaction) {
            throw $this->createNotFoundException("Unable to find Interaction [$id].");
        }

        $jsonEntity = $this->serializeEntity($interaction);
        $object = $interaction->getObject()->getDisplayName();
        $subject = $interaction->getSubject()->getDisplayName();
        $tags = $interaction->getTagNames();
        $type = $interaction->getInteractionType()->getActiveForm();

        return $this->render('Entity/interaction.html.twig', array(
            'entity' => $jsonEntity,
            'object' => $object,
            'show' => 'interaction',
            'subject' => $subject,
            'tags' => $tags,
            'type' => $type,
        ));
    }
/* ----------------------------- TAXON -------------------------------- */
    /**
     * Opens the Taxon show page.
     *
     * @Route("taxon/{id}", name="app_taxon_show")
     */
    public function showTaxonAction($id)
    {
        $taxon = $this->getEntity('Taxon', $id);
        if (!$taxon) {
            throw $this->createNotFoundException("Unable to find Taxon [$id].");
        }

        $jsonEntity = $this->serializeEntity($taxon);

        return $this->render('Entity/taxon.html.twig', array(
            'displayName' => $taxon->getDisplayName(),
            'entity' => $jsonEntity,
            'show' => 'taxon',
        ));
    }

}