<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Site Statistics controller.
 *
 * @Route("/stats")
 */
class SiteStatisticsController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Returns statistics for the passed data group.
     *
     * @Route("/", name="app_page_stats")
     */
    public function pageStatisticsAction(Request $request)
    {

        $requestContent = $request->getContent();
        $tag = json_decode($requestContent)->tag;
        $data = $this->getPageStats($tag);

        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }
    private function getPageStats($tag)
    {
        $map = [
            "all" => function() { return $this->buildAllStatData(); },
            "core" => function() { return $this->buildCoreStatData(); },
            "db" => function() { return $this->buildAboutDatabaseStatData(); },
            "project" => function() { return $this->buildAboutProjectStatData(); },
        ];
        return call_user_func($map[$tag]);
    }

    private function buildAboutProjectStatData()
    {
        $users = $this->em->getRepository('App:User')->findAll();
        $editors = 0;

        foreach ($users as $user) {
            if (!$user->hasRole('ROLE_EDITOR')) { continue; }
            $editors++;
        }
        return [
            'editor' => $editors,
            'usr' => count($users),
        ];
    }

    private function buildAboutDatabaseStatData()
    {
        $data = $this->buildCoreStatData();
        $data['nonBats'] = $this->getAllNonBatSpeciesCount($data['bats']);
        return $data;
    }

    private function buildCoreStatData()
    {
        $locs = $this->getLocationsWithInteractions();

        return [
            'bats' => $this->getBatSpeciesCount(),
            'cits' => count($this->em->getRepository('App:Source')->findDirectSources()),
            'cntries' => $locs['cntries'],
            'ints' => count($this->em->getRepository('App:Interaction')->findAll()),
            'locs' => $locs['places'],
        ];
    }

    private function buildAllStatData()
    {
        $project = $this->buildAboutProjectStatData();
        $db = $this->buildAboutDatabaseStatData();
        return array_merge($project, $db);
    }

/* =========================== GET COUNTS =================================== */
/* ---------------------------- LOCATIONS =---------------------------------- */
    private function getLocationsWithInteractions()
    {
        $data = [ 'places' => [/*names*/], 'cntries' => [/*names*/] ];
        $locs = $this->em->getRepository('App:Location')->findAll();

        foreach ($locs as $loc) {
            if ($this->ifHabitatOrNoInteractions($loc)) { continue; }
            $this->addPlaceName($data['places'], $loc->getDisplayName());
            $this->addCountry($data['cntries'], $loc->getCountryData());
        }
        return $this->returnCounts($data);
    }

    private function ifHabitatOrNoInteractions($loc)
    {
        $isHabitat = $loc->getLocationType()->getDisplayName() === 'Habitat';
        return !count($loc->getInteractions()) || $isHabitat;
    }

    private function addCountry(&$ary, $cntry)
    {
        if (!$cntry) { return; }
        $this->addPlaceName($ary, $cntry['displayName']);
    }

    private function getCountryCount(&$locs)
    {
        $cntries = $locs['cntries'];
        $locs = $locs['places'];
        return $cntries;
    }
    private function addPlaceName(&$ary, $name)
    {
        array_push($ary, $name);
    }
    private function returnCounts($data)
    {
        $data['cntries'] = count(array_unique($data['cntries']));
        $data['places'] = count(array_unique($data['places']));
        return $data;
    }
/* --------------------------- BAT SPECIES ---------------------------------- */
    private function getBatSpeciesCount()
    {
        $speciesCount = 0;
        $bat = $this->em->getRepository('App:Taxon')->findOneBy(['name' => 'Chiroptera']);

        $speciesCount += $this->getSpeciesTaxa($bat);
        return $speciesCount;
    }

    private function getSpeciesTaxa($taxon)
    {
        if ($taxon->getRank()->getDisplayName() === 'Species') { return 1; }
        $subCount = 0;

        foreach ($taxon->getChildTaxa() as $childTaxon) {
            $subCount += $this->getSpeciesTaxa($childTaxon);
        }

        return $subCount;
    }
/* ------------------------- OTHER SPECIES ---------------------------------- */
    private function getAllNonBatSpeciesCount($batSpeciesCount)
    {
        $species = $this->em->getRepository('App:Rank')
            ->findOneBy(['displayName' => 'Species']);

        return count($species->getTaxa()) - $batSpeciesCount;
    }

}
