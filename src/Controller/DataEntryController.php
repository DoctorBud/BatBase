<?php

namespace App\Controller;

use App\Service\SerializeData;
use App\Service\DataManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Date-entry/edit form controller.
 *
 * @Route("/crud", name="app_")
 */
class DataEntryController extends AbstractController
{
    private $dataManager;
    private $em;
    private $serialize;

    public function __construct(
        DataManager $dataManager,
        EntityManagerInterface $em,
        SerializeData $serialize)
    {
        $this->dataManager = $dataManager;
        $this->em = $em;
        $this->serialize = $serialize;
    }
        /** Returns the entity. */
    private function getEntity($class, $val, $prop = 'id')
    {
        return $this->em->getRepository("App:".$class)
            ->findOneBy([$prop => $val]);
    }
/* ========================== CREATE ENTITY ================================= */
    /**
     * Creates a new Entity, and any new detail-entities, from the form data.
     *
     * @Route("/entity/create", name="entity_create")
     */
    public function entityCreateAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        $this->denyAccessUnlessGranted('ROLE_EDITOR');
        $formData = json_decode($request->getContent());                               //print("\nForm data =");print_r($formData);
        $coreName = $formData->coreEntity;                                      //print("coreName = ". $coreName);

        $returnData = $this->dataManager->createEntity($coreName, $formData);

        return $this->sendDataAndResponse($returnData);
    }
/* ========================== DELETE ENTITY ================================= */
    /**
     * Soft-Deletes entity. Currently, only interactions by admin users.
     *
     * @Route("/entity/delete", name="entity_delete")
     */
    public function entityDeleteAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $formData = json_decode($request->getContent());                               //print("\nForm data =");print_r($formData);
        $coreName = $formData->coreEntity;                                      //print("coreName = ". $coreName);

        $returnData = $this->dataManager->deleteEntity($coreName, $formData->id);

        return $this->sendDataAndResponse($returnData);
    }
/* ========================== EDIT ENTITY =================================== */
    /**
     * Edits entity, reporting any errors or returning the serialized results.
     * The database transaction is cancelled, leaving the DB unmodified, and the
     * form-data is quarantined, pending review and approval by a data manager.
     *
     * @Route("/entity/edit", name="entity_edit")
     */
    public function entityEditAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        $this->denyAccessUnlessGranted('ROLE_EDITOR');

        $formData = json_decode($request->getContent());                               //print("\nForm data =");print_r($formData);
        $coreName = $formData->coreEntity;                                      //print("coreName = ". $coreName);

        $returnData = $this->dataManager->editEntity($coreName, $formData);

        return $this->sendDataAndResponse($returnData);
    }
/* ====================== QUARANTINE DATA-ENTRY ============================= */
    /**
     * Creates/edits data, reporting any errors or returning the serialized results.
     * The database transaction is cancelled, leaving the DB unmodified, and the
     * form-data is quarantined, pending review and approval by a data manager.
     *
     * @Route("/entity/pend", name="entity_pend_entry")
     */
    public function quarantineDataEntry(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        $this->denyAccessUnlessGranted('ROLE_EDITOR');

        $this->em->getConnection()->beginTransaction(); // suspend auto-commit

        $data = json_decode($request->getContent());
        $pend = $data->pend;
        $returnData = $this->getQuarantinedEntityData($data, $pend->action);
        $this->dataManager->quarantineData($pend, $data);
        if (property_exists($pend, 'errors')) { $returnData->quarantineErrors = $pend->errors; }
        return $this->sendDataAndResponse($returnData);
    }
    private function getQuarantinedEntityData($formData, $action)
    {
        $coreName = $formData->coreEntity;
        $returnData = null;

        try {
            if ($action === 'create') {
                $returnData = $this->dataManager->createEntity($coreName, $formData);
            } else {
                $returnData = $this->dataManager->editEntity($coreName, $formData);
            }
            $returnData->quarantined = true;
            throw new \Exception('Data quarantined until approved.');
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
        }

        $this->em->getConnection()->setAutoCommit(true);
        return $returnData;
    }
/* ====================== UPDATE CITATION TEXT ============================== */
    /**
     * Updates the Citation and Source entities with the updated citation text.
     *
     * @Route("/citation/edit", name="citation_edit")
     */
    public function citationTextupdate(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        $this->denyAccessUnlessGranted('ROLE_EDITOR');
        $data = json_decode($request->getContent());

        $returnData = $this->dataManager->updateCitationText($data);

        return $this->sendDataAndResponse($returnData);
    }
/* ======================= SERIALIZE AND RETURN ============================= */
    /** Sends an object with the entities' serialized data back to the crud form. */
    private function sendDataAndResponse($entityData)
    {
        $serialize = ['core', 'detail'];

        foreach ($serialize as $p) {
            $prop = $p.'Entity';
            $id = $p.'Id';
            if (!property_exists($entityData, $prop) || !$entityData->$prop ) { continue; }
            try {
                $entityData->$id = $entityData->$prop->getId();
                $entityData->$prop = $this->serialize->serializeRecord(
                    $entityData->$prop, 'normalized');
            } catch (\Throwable $e) {
                return $this->sendErrorResponse($e);
            } catch (\Exception $e) {
                return $this->sendErrorResponse($e);
            }
        }
        $response = new JsonResponse();
        $response->setData(array(
            'results' => $entityData
        ));
        return $response;
    }
    /** Logs the error message and returns an error response message. */
    private function sendErrorResponse($e)
    {
        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData($e->getMessage());
        return $response;
    }
}