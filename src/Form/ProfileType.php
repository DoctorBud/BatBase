<?php

namespace App\Form;

use App\Entity\User;
use App\Form\ShowHidePasswordType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => 'Email'])
            ->add('username', TextType::class, ['label' => 'Username'])
            ->add('first_name', TextType::class, ['label' => 'First Name'])
            ->add('last_name', TextType::class, ['label' => 'Last Name'])
            ->add('about_me', TextareaType::class, ['label' => 'About Me'])
            ->add('interest', TextType::class, ['label' => 'Interest Area', 'required' => false])
            ->add('country', TextType::class, ['label' => 'Country', 'required' => false])
            ->add('education', TextType::class, ['label' => 'Education Level', 'required' => false])
            ->add('captcha', EWZRecaptchaType::class, [
                'mapped'      => false,
                'constraints' => [new RecaptchaTrue()],
                'attr' => [
                    'options' => [
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal',
                        'defer' => true,
                        'async' => true,
                        // callback will be set by default if not defined (along
                        // with JS function that validate the form on success)
                        'callback' => 'onReCaptchaSuccess',
                    ]
                ],
            ])
            ->add('password', ShowHidePasswordType::class, ['label' => 'Password'])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
