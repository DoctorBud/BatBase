<?php

namespace App\Service;

use App\Service\Mailer;
use App\Service\TrackEntityUpdate;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;

/**
 * Builds a summary of data changes and emails the digest to site admin.
 *
 * TOC
 *     COMPOSE EMAIL
 *         BUILD
 *         SEND
 *     BUILD DIGEST DATA
 *         DATA ENTRY
 *         SUBMITTED PUBLICATIONS
 *         NEW USER
 *         USER FEEDBACK
 *         HELPERS
 */
class WeeklyDigestManager
{
    private $em;
    private $logger;
    private $mailer;
    private $oneWeekAgo;
    private $rootPath;
    private $tracker;

    public function __construct(string $rootPath, EntityManagerInterface $em,
        LoggerInterface $logger, Mailer $mailer, TrackEntityUpdate $tracker)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->rootPath = $rootPath;
        $this->tracker = $tracker;
    }
/* ======================== COMPOSE EMAIL =================================== */
    /**
     * Sends an email to site admin's with various updates from site activity
     * over the last week: Data entry, submitted publications, new users, and new
     * user feedback.
     */
    public function sendAdminWeeklyDigestEmail()
    {
        $data = $this->buildWeeklyDigestData();
        $users = $this->em->getRepository('App:User')->findAdmins();

        foreach ($users as $user) {
            $email = $this->getDigestEmail($user, $data);
            $this->handleSendEmail($email);
        }
        return $data;
    }
/* ----------------------- BUILD -------------------------------------------- */
    /**
     * Build the email object with the weekly digest data and any submitted PDFs.
     *
     * @param string $user User
     * @param array  $data Digest data
     *
     * @return Email
     */
    private function getDigestEmail($user, $data)
    {
        $email = $this->initDigestEmail($this->getAddress($user));
        $this->handleEmailBody($data, $email);
        $this->attachNewPDFs($data['pdfs'], $email);
        return $email;
    }
    private function getAddress($user)
    {
        // defining the email address and name as an object
        // (email clients will display the name)
        return new Address($user->getEmail(), $user->getFirstName());
    }
    /**
     * Inits and returns the Swiftmailer email.
     *
     * @param string $address Admin Address
     *
     * @return Email
     */
    private function initDigestEmail($address)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@batbase.org')
            ->to($address)
            ->subject('BatBase Weekly Digest');
        return $email;
    }
    /**
     * Attach PDF files to the email.
     *
     * @param array $pdfs  PDF data
     * @param Class $email Email
     */
    private function attachNewPDFs($pdfs, &$email): void
    {
        foreach ($pdfs as $pdf) {
            $path = $this->rootPath.'/public_html/'.$pdf['path'];
            $email->attachFromPath($path, $pdf['title']);
        }
    }
    private function handleEmailBody($data, &$email): void
    {
        $template = 'emails/weekly-digest.html.twig';
        $email
            ->htmlTemplate($template)
            ->context($data);
    }
/* ----------------------- SEND --------------------------------------------- */
    private function handleSendEmail($email): void
    {
        $this->mailer->sendEmail($email);
    }
/* ======================== BUILD DIGEST DATA =============================== */
    /**
     * Returns data about site activity over the last week: Data-entry, submitted
     * publications, new users, and new user fedeback.
     * @return array          Weekly digest data
     */
    private function buildWeeklyDigestData()
    {
        $this->oneWeekAgo = $this->getDate('-7 days');
        return $this->getWeeklyDigestData();
    }
    private function getDate($timeframe)
    {
        return new \DateTime($timeframe, new \DateTimeZone('UTC'));
    }
    private function getWeeklyDigestData()
    {
        $data = [
            'lastWeek' => $this->oneWeekAgo->format('m-d-Y'),
            'yesterday' => $this->getDate('-7 days')->format('m-d-Y'),
            'dataUpdates' => $this->getUpdatedEntityData(),
            'feedback' => $this->getFeedbackData(),
            'pdfs' => $this->getPDFData(),
            'users' => $this->getNewUserData(),
        ];                                                                      print_r($data);
        return $data;
    }
/* ----------------------- DATA ENTRY --------------------------------------- */
    /**
     * @return array  Each entity with updates, wth the totals created and edited.
     */
    private function getUpdatedEntityData()
    {
        $data = [];

        foreach ($this->getUpdated('SystemDate') as $updated) {
            $entityName = $updated->getEntity();
            if ($this->isNotIncluded($entityName)) { continue; }
            array_push($data, $this->countUpdates($entityName));
        }
        return $data;
    }
    private function isNotIncluded($entityName)
    {
        $skip = ['System', 'FileUpload', 'Feedback', 'Source', 'GeoJson'];
        return in_array($entityName, $skip);
    }
    private function countUpdates($entityName)
    {
        return [
            'name' => $entityName,
            'created' => count($this->getCreated($entityName)),
            'updated' => count($this->getUpdated($entityName))
        ];
    }
/* ------------------- SUBMITTED PUBLICATIONS ------------------------------- */
    /**
     * @return array  Data for each publication submitted last week
     */
    private function getPDFData()
    {
        $data = [];
        foreach ($this->getCreated('FileUpload') as $pdf) {
            array_push(
                $data, [
                    'path' => $pdf->getPath().$pdf->getFileName(),
                    'filename' => $pdf->getFileName(),
                    'title' => $pdf->getTitle(),
                    'description' => $pdf->getDescription(),
                    'user' => $pdf->getCreatedBy()->getFullName(),
                    'userEmail' => $pdf->getCreatedBy()->getEmail()
                ]
            );
        }
        return $data;
    }
/* ----------------------- NEW USER ----------------------------------------- */
    /**
     * @return array  Data for each new user from last week
     */
    private function getNewUserData()
    {
        $data = [];
        foreach ($this->getCreated('User') as $user) {
            array_push($data, [
                'name' => $user->getFullName(),
                'about' => $user->getAboutMe()
            ]);
        }
        return $data;
    }
/* ----------------------- USER FEEDBACK ------------------------------------ */
    /**
     * NOTE: Not currently reporting updates to feedback.
     * @return array  Data for user feedback submitted last week
     */
    private function getFeedbackData()
    {
        $created = $this->getCreated('Feedback');
        $updated = $this->getUpdated('Feedback');

        $data = [
            // 'created' => $this->getCreatedFeedback($created),
            // 'updated' => $this->getCreatedFeedback($updated)
        ];
        return $data;
    }
    // private function getCreatedFeedback($created)
    // {
    //     $data = [];
    //     foreach ($created as $feedback) {
    //         array_push($data, [
    //             'page' => $feedback->getRoute(),
    //             'topic' => $feedback->getTopic(),
    //             'feedback' => $feedback->getContent(),
    //             'user' => $feedback->getCreatedBy()->getFullName(),
    //             'userEmail' => $feedback->getCreatedBy()->getEmail()
    //         ]);
    //     }
    //     return $data;
    // }
    // private function getUpdatedFeedback($updated)
    // {
    //     $data = [];
    //     foreach ($created as $feedback) {
    //         array_push($data, [
    //             'status' => $feedback->getStatusStr(),
    //             'assignedTo' => $feedback->assignedUser->getFullName(),
    //             'notes' => $feedback->getAdminNotes(),
    //             'page' => $feedback->getRoute(),
    //             'topic' => $feedback->getTopic(),
    //             'feedback' => $feedback->getContent(),
    //             'user' => $feedback->getCreatedBy()->getFullName(),
    //             'userEmail' => $feedback->getCreatedBy()->getEmail()
    //         ]);
    //     }
    //     return $data;
    // }
/* ------------------------ HELPERS ----------------------------------------- */
    private function getUpdated($entityName)
    {
        return $this->tracker->getUpdatedSince($entityName, $this->oneWeekAgo);
    }
    private function getCreated($entityName)
    {
        return $this->tracker->getCreatedSince($entityName, $this->oneWeekAgo);
    }
}