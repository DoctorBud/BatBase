<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
/**
 * public:
 *
 * TOC:
 *     TRACK TIME-UPDATED
 *     GET UPDATED ENTITIES
 */
class TrackEntityUpdate
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
/* ======================== TRACK TIME-UPDATED ============================== */
    /**
     * Track the time entity-data is modified.
     */
    public function trackEntityUpdate($entityName)
    {
        $entity = $this->getEntityDate($entityName);
        if (!$entity) { return; }
        $this->trackUpdateAndPersist($entity);
        /* Update system-updated time. */
        $system = $this->getEntityDate('System');
        $this->trackUpdateAndPersist($system);
        $this->em->flush();
    }
    private function getEntityDate($name)
    {
        return $this->em->getRepository('App:SystemDate')
            ->findOneBy(['entity' => $name]);
    }
    private function trackUpdateAndPersist($entity)
    {
        $entity->setUpdated(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->em->persist($entity);
    }
/* ====================== GET UPDATED ENTITIES ============================== */
    public function getCreatedSince($entity, $date)
    {
        $repo = $this->em->getRepository('App:'.$entity);
        $query = $repo->createQueryBuilder('e')
            ->where('e.created > :date')
            ->setParameter('date', $date)
            ->getQuery();
        return $query->getResult();
    }
    public function getUpdatedSince($entity, $date)
    {
        $repo = $this->em->getRepository('App:'.$entity);
        $query = $repo->createQueryBuilder('e')
            ->where('e.updated > :date');
        if ($entity !== 'SystemDate') {
            $query->andWhere('e.updated > e.created');
        }
        $query->setParameter('date', $date);
        return $query->getQuery()->getResult();
    }
}