<?php
namespace App\EventSubscriber;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use App\Service\UserActivity;

/**
 * Listener that updates the last activity of the authenticated user
 */
class ActivityListener
{
    protected $tokenContext;
    private $userActivity;

    public function __construct($tokenContext, UserActivity $userActivity)
    {
        $this->tokenContext= $tokenContext;
        $this->userActivity = $userActivity;
    }

    /**
    * Update the user "lastActivity" on each request
    * @param ControllerEvent $event
    */
    public function onCoreController(ControllerEvent $event)
    {
        // Check that the current request is a "MASTER_REQUEST"
        // Ignore any sub-request
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }
        // Check token authentication availability
        if ($this->tokenContext->getToken()) {
            $this->userActivity->track();
        }
    }
}