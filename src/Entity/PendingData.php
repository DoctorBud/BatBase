<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * Pending Data
 * - Data-Entry by Contributors must be reviewed and approved.
 *
 * @ORM\Table(name="pending_data")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
  */
class PendingData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * 0: Pending, 1: Locked, 2: Returned, 3: Held
     * @var string
     *
     * @ORM\Column(name="stage", type="integer", nullable=false)
     * @JMS\Expose
     */
    private $stage;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255, nullable=false)
     * @JMS\Expose
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_id", type="string", length=255, nullable=true)
     * @JMS\Expose
     * @JMS\SerializedName("entityId")
     */
    private $entity_id;

    /**
     * JSON data string
     * @var string
     *
     * @ORM\Column(name="data", type="text", nullable=false)
     * @JMS\Expose
     */
    private $data;

    /**
     * When data is locked/held by a data-manager.
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="managed_by", referencedColumnName="id", nullable=true)
     */
    private $managedBy;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     * @JMS\Expose
     * @JMS\SerializedName("serverUpdatedAt")
     */
    private $updated;

    /**
     * @var User
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * Get id.
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stage.
     *
     * @param string $stage
     *
     * @return PendingData
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage.
     *
     * @return string
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set entity.
     *
     * @param string $entity
     *
     * @return PendingData
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity.
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set entityId.
     *
     * @param string $entityId
     *
     * @return PendingData
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId.
     *
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }


    /**
     * Set data.
     *
     * @param string $data
     *
     * @return PendingData
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set managedBy user.
     *
     * @return \App\Entity\User
     */
    public function setManagedBy(\App\Entity\User $user)
    {
        $this->managedBy = $user;
    }

    /**
     * Get managedBy user.
     *
     * @return \App\Entity\User
     */
    public function getManagedBy()
    {
        return $this->managedBy;
    }

    /**
     * Get managedBy user ID.
     * @JMS\VirtualProperty
     * @JMS\SerializedName("managedBy")
     *
     * @return \App\Entity\User
     */
    public function getManagedById()
    {
        return $this->managedBy;
    }

    /**
     * Set managedBy user.
     *
     * @return \App\Entity\User
     */
    public function setCreatedBy(\App\Entity\User $user)
    {
        $this->createdBy = $user;
    }

    /**
     * Get created datetime.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get createdBy user.
     *
     * @return \App\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Get Created by User Id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("user")
     *
     * @param int
     */
    public function getCreatedById()
    {
        return $this->createdBy->getId();
    }

    /**
     * Set last updated by user.
     *
     * @return \App\Entity\User
     */
    public function setUpdatedBy(\App\Entity\User $user = null)
    {
        $this->updatedBy = $user;
    }

    /**
     * Get last updated datetime.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Get last updated by user.
     *
     * @return \App\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Get string representation of object.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getData();
    }
}
