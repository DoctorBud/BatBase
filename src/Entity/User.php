<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\LegacyPasswordAuthenticatedUserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, LegacyPasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /* ====================== USER DETAILS ================================== */
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     * @JMS\Expose
     * @JMS\SerializedName("firstName")
     * @Groups({"flattened"})
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     * @JMS\Expose
     * @JMS\SerializedName("lastName")
     * @Groups({"flattened"})
     */
    protected $lastName;

    /**
     * @var string
     * @ORM\Column(name="about_me", type="text")
     *
     * @Assert\NotBlank(message="Tell us about your interest in bat eco-interactions")
     * @Assert\Length(
     *     min=2,
     *     max="255",
     *     minMessage="Tell us more about your interest in bat eco-interactions",
     *     maxMessage="Limit of 255 characters exceded"
     * )
     * @JMS\Expose
     * @JMS\SerializedName("aboutMe")
     * @Groups({"flattened"})
     */
    protected $aboutMe;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Expose
     * @JMS\SerializedName("education")
     */
    protected $education;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Expose
     * @JMS\SerializedName("country")
     * @Groups({"flattened"})
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Expose
     * @JMS\SerializedName("interest")
     * @Groups({"flattened"})
     */
    protected $interest;
    /* ====================== USER IDENTIFIERS ============================== */
    /**
     * @ORM\Column(type="string", length=180)
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @var string
     */
    protected $emailCanonical;

    /**
     * @ORM\Column(type="string", length=180)
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @var string
     */
    protected $usernameCanonical;

    /**
     * @ORM\Column(type="string")
     * @Assert\Length(
     *     min=8,
     *     max="255",
     *     minMessage="Password must be at least 8 characters",
     *     maxMessage="Limit of 255 characters exceded"
     * )
     * @var string The hashed password
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    public $plainPassword;

    /**
     * The salt to use for hashing.
     *
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $salt;

    /**
     * @ORM\Column(type="array")
     */
    protected $roles = [];

    protected const ROLE_DEFAULT = 'ROLE_USER';

    protected const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    /* ====================== USER FORENSIC ================================= */
    /**
     * Random string sent to the user email address in order to verify it.
     *
     * @ORM\Column(type="string", length=180, unique=true, nullable=true)
     * @var string|null
     */
    protected $confirmationToken;

    /**
     * @ORM\Column(name="is_verified", type="boolean", nullable=false)
     * @var bool
     */
    protected $isVerified;

    /**
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     * @var \DateTime|null
     */
    protected $passwordRequestedAt;

    /**
     * Date/Time of the last activity
     *
     * @var \DateTime
     * @ORM\Column(name="last_activity_at", type="datetime", nullable=true)
     */
    protected $lastActivityAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->isVerified = false;
        $this->roles = [];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }
/* ====================== SET/GET DETAILS =================================== */
    /* ----------------------- NAME ----------------------------------------- */
    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Get fullName.
     * @JMS\VirtualProperty
     * @JMS\SerializedName("fullName")
     * @Groups({"flattened"})
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }
    /* ----------------------- ABOUT ---------------------------------------- */
    /**
     * Set aboutMe.
     *
     * @param string $aboutMe
     *
     * @return User
     */
    public function setAboutMe($aboutMe)
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }

    /**
     * Get aboutMe.
     *
     * @return string
     */
    public function getAboutMe(): ?string
    {
        return $this->aboutMe;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * Set education.
     *
     * @param string $education
     *
     * @return User
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education.
     *
     * @return string
     */
    public function getEducation(): ?string
    {
        return $this->education;
    }

    /**
     * Set interest.
     *
     * @param string $interest
     *
     * @return User
     */
    public function setInterest($interest)
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * Get interest.
     *
     * @return string
     */
    public function getInterest(): ?string
    {
        return $this->interest;
    }
/* ====================== GET/SET IDENTIFIERS =============================== */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        $this->emailCanonical = $this->canonicalize($email);

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;
        $this->usernameCanonical = $this->canonicalize($username);

        return $this;
    }

    /**
     * From the FOSUserBundle package.
     */
    public function canonicalize($string)
    {
        if (null === $string) {
            return;
        }

        $encoding = mb_detect_encoding($string);
        $result = $encoding
            ? mb_convert_case($string, MB_CASE_LOWER, $encoding)
            : mb_convert_case($string, MB_CASE_LOWER);

        return $result;
    }

    /**
     * Phasing out...
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @return string the hashed password for this user
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        $this->salt = null; //No longer used in modern hashing algorithms

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Get user roles.
     * @JMS\VirtualProperty
     * @JMS\SerializedName("roles")
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }


    public function setRoles(array $roles): self
    {
        $this->roles = [];

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function addRole($role)
    {
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }
    public function hasRole(string $role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

/* ====================== GET/SET FORENSIC ================================== */
    /**
     * If you store any temporary, sensitive data on the user, clear it here.
     *
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @param \DateTime $lastActivityAt
     */
    public function setLastActivityAt($lastActivityAt)
    {
        $this->lastActivityAt = $lastActivityAt;
    }

    /**
     * @return \DateTime
     */
    public function getLastActivityAt()
    {
        return $this->lastActivityAt;
    }

    /**
     * @return Bool Whether the user is active or not
     */
    public function isActiveNow()
    {   // Delay during wich the user will be considered as still active
        $delay = new \DateTime('2 minutes ago', new \DateTimeZone('UTC'));
        return ( $this->getLastActivityAt() > $delay );
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Get created datetime.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get last updated datetime.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Get deleted at.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set deleted at.
     *
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUsername();
    }
}
