<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * TaxonRepository.
 */
class TaxonRepository extends EntityRepository
{
    public function findAllNonBatTaxa($batGroup)
    {
        return $this->createQueryBuilder('taxon')
            ->leftJoin('taxon.group', 'group_taxon')
            ->andWhere('group_taxon.group != :batGroup')
            ->setParameter('batGroup', $batGroup)
            ->getQuery()
            ->execute();
    }
}
