<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ContentBlockRepository.
 */
class ContentBlockRepository extends EntityRepository
{
    public function findBySlugs($slugAry)
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT c FROM App:ContentBlock c WHERE c.slug IN (:slugAry)'
            );
        $query->setParameter('slugAry', $slugAry);

        return $query->getResult();
    }
}
