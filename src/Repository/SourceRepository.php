<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Source Repository.
 */
class SourceRepository extends EntityRepository
{
    public function findDirectSources()
    {
        return $this->createQueryBuilder('s')
            ->where('s.isDirect = :isDirect')
            ->setParameter('isDirect', true)
            ->getQuery()
            ->getResult();
    }
}
