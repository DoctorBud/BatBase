<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\Group;
use App\Entity\GroupRoot;
use App\Entity\Taxon;
use App\Entity\Tag;
use App\Entity\ValidInteraction;
use App\Service\DataManager;

/**
 * Adds a ValidInteraction entity for Bat->Bat predation/prey interactions
 */
final class Version20220604MissingValidInteraction extends AbstractMigration implements ContainerAwareInterface
{

    private $em;
    protected $container;
    protected $dataManager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function setDataManager(DataManager $manager)
    {
        $this->dataManager = $manager;
    }

    public function getDescription() : string
    {
        return 'Adds a ValidInteraction entity for Bat->Bat predation/prey interactions.';
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    private function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    private function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
        }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }

    private function create($cName, $data)
    {
        $entityData = $this->dataManager->new($cName, $data);
        return $entityData->coreEntity;
    }

    private function edit($cName, $data)
    {
        return $this->dataManager->editEntity($cName, $data);
    }

/* ============================== UP ======================================== */
    public function up(Schema $schema) : void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6, 'id');

        $this->createValidInteractions();
        $this->em->flush();
    }
/* +++++++++++++++++++ VALID INTERACTIONS +++++++++++++++++++++++++++++++++++ */
    private function createValidInteractions()
    {
        $bat = $this->getGroupRoots('Bat');
        $this->createPredationInteractions($bat);
    }
/* --------------------------- PREDATION ------------------------------------ */
    private function createPredationInteractions($bat)
    {
        $this->handleCreate($bat, $bat, 'Predation', [], false);
        $this->handleCreate($bat, $bat, 'Prey', [], false);
    }
/* ----------------------- CREATE ENTITIES ---------------------------------- */
    private function handleCreate($sName, $oName, $type, $tags, $tRequired)
    {
        $subjs = $this->getGroupRoots($sName);
        $type = $this->getEntity('InteractionType', $type, 'displayName');
        $objs = $this->getGroupRoots($oName);

        foreach ($subjs as $subj) {
            foreach ($objs as $obj) {
                $this->createValidInteraction($subj, $obj, $type, $tags, $tRequired);
            }
        }

    }
    private function getGroupRoots($gName)
    {
        if (!is_string($gName)) { return $gName; }                          print("\n".$gName);
        $group = $this->getEntity('Group', $gName, 'displayName');
        return $group->getTaxa();
    }
    private function createValidInteraction($subj, $obj, $type, $tags, $tRequired)
    {
        $entity = new ValidInteraction();
        $entity->setSubjectGroupRoot($subj);
        $entity->setObjectGroupRoot($obj);
        $entity->setInteractionType($type);
        $entity->setTagRequired($tRequired);

        foreach ($tags as $tag) {
            $entity->addTag($tag);
        }
        $this->persistEntity($entity, true);
    }
/* ============================ DOWN ======================================== */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}