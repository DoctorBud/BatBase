<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Service\DataManager;

/**
 * Replaces 'view' combo values with the combo text
 * Deletes test data mistakenly added by an editor
 */
final class Version20220216FilterSetCleanup extends AbstractMigration implements ContainerAwareInterface
{

    private $em;
    protected $container;
    protected $dataManager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function setDataManager(DataManager $manager)
    {
        $this->dataManager = $manager;
    }

    public function getDescription() : string
    {
        return 'Replaces "view" combo values with the combo text
            Replaces ObjectRealmFilter';
    }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    private function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    private function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
        }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }
/* ============================== UP ======================================== */
    public function up(Schema $schema) : void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6, 'id');

        $this->updateFilterSets();
        // $this->cleanUpData();

        $this->em->flush();
    }
/* +++++++++++++++++++++++++ FILTER SETS ++++++++++++++++++++++++++++++++++++ */
    private function updateFilterSets()
    {
        $sets = $this->getAllFilterSets();
        foreach ($sets as $set) {
            $details = json_decode($set->getDetails());                         //print("\n details = "); print_r($details);
            if (!$details->view) { continue; }
            $this->checkForComboFilterUpdate($details);
            $view = $this->getViewText($set, $details->view);                   //print('new view'); print_r($view);
            $details->view = $view;
            $set->setDetails(json_encode($details));
            $this->persistEntity($set);
        }
    }
    //remove spaces from all filters
    private function checkForComboFilterUpdate(&$details)
    {
        if (!property_exists($details, 'direct')) { return; }
        if (!property_exists($details->direct, 'combo')) { return; }
        $this->updateObjectRealm($details);
        $this->updatePublicationType($details);
    }
    private function updateObjectRealm(&$details)
    {
        if (!property_exists($details->direct->combo, 'Object Realm')) { return; }
        $details->direct->combo->{"Taxon Group"} = $details->direct->combo->{"Object Realm"};
        unset($details->direct->combo->{"Object Realm"});
    }
    private function updatePublicationType(&$details)
    {
        if (!property_exists($details->direct->combo, 'Publication Type')) { return; }
        $details->direct->combo->{"PublicationType"} = $details->direct->combo->{"Publication Type"};
        unset($details->direct->combo->{"Publication Type"});
    }
    private function getViewText($set, $val)
    {
        $this->ifViewTaxonIdAndShouldBeGroupRoot($set, $val);
        $map = [
            'srcs' => 'Source',
            'auths' => 'Author',
            'pubs' => 'Publication',
            'publ' => 'Publisher',
            'taxa' => 'Taxon',
            '1' => 'Bats',
            '2' => 'Plants',
            '3' => 'Arthropod',
            '4' => 'Bacteria',
            '5' => 'Fishes',
            '6' => 'Fungi',
            '7' => 'Viruses',
            '8' => 'Birds',
            '9' => 'Reptiles',
            '10' => 'Amphibians',
            '12' => 'Mammals',
            '13' => 'Worms',
            '14' => 'Chromista'
        ];
        return [ 'text' => $map[$val] ,'value' => $val ];
    }
    private function ifViewTaxonIdAndShouldBeGroupRoot($set, &$val)
    {
        if (str_contains($set->getDisplayName(), '(Exmpl) Taxon')) { $val = 1; }
        $updates = [
            1325 => 1,
            2572 => 1,
            2468 => 1,
            1620 => 1,
            2180 => 3,
            2186 => 1,
            663 => 3,
            1324 => 1,
            2541 => 1
        ];
        if (!array_key_exists($set->getId(), $updates)) { return; }
        $val = $updates[$set->getId()];
    }
    private function getAllFilterSets()
    {
        $lists = $this->getEntities('UserNamed');
        $return = [];
        foreach ($lists as $list) {  //print('type = '.$list->getType());
            if ($list->getType() !== 'filter') { continue; }
            array_push($return, $list);
        }                                                                       print('total fitler sets = '.count($return));
        return $return;
    }
/* ++++++++++++++++++++++++ DATA CLEANUP ++++++++++++++++++++++++++++++++++++ */
    private function cleanUpData()
    {
        // code...
    }
/* ============================ DOWN ======================================== */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}