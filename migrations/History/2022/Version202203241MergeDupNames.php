<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

// use App\Entity\InteractionType;
use App\Service\DataManager;


/**
 * Deletes all duplicate taxa with the same name and parent, merging all
 * interactions to the first taxon that was created.
 */
final class Version202203241MergeDupNames extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    private $em;
    private $admin;
    // private $dataManager;

    public function getDescription(): string
    {
        return 'Deletes all duplicate taxa with the same name and parent, merging all interactions to the first taxon that was created.';
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    // public function setDataManager(DataManager $manager)
    // {
    //     $this->dataManager = $manager;
    // }

    private function getEntity($className, $val, $prop = 'id')
    {
        return $this->em->getRepository('App:'.$className)
            ->findOneBy([$prop => $val]);
    }

    public function getEntities($className)
    {
        return $this->em->getRepository('App:'.$className)->findAll();
    }

    public function persistEntity($entity, $creating = false)
    {
        if ($creating) {
            $entity->setCreatedBy($this->admin);
        }
        $entity->setUpdatedBy($this->admin);
        $this->em->persist($entity);
    }
/* ========================== up ============================================ */

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema):void
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->getEntity('User', 6);

        $this->deleteAndMergeDuplicateSrcs();
        $this->deleteAndMergeDuplicateTaxa();
        $this->em->flush();
    }

    private function sortTaxabyName($taxa)
    {
        $sorted = [];

        foreach ($taxa as $txn) {
            if (!array_key_exists($txn->getDisplayName(), $sorted)) {
                $sorted[$txn->getDisplayName()] = [];
            }
            array_push($sorted[$txn->getDisplayName()], $txn);
        }

        return $sorted;
    }

    private function deleteAndMergeDuplicateTaxa()
    {
        $entities = $this->sortTaxabyName($this->getEntities('Taxon'));
        $removed = 0;
        foreach ($entities as $taxa) {
            if (count($taxa) === 1) { continue; }
            $txn = array_shift($taxa);
            $this->mergeDataAndDeleteTaxa($txn, $taxa);
            $removed += count($taxa);
        }                                                                       print("\nTOTAL REMOVED = ".$removed);
    }
    private function mergeDataAndDeleteTaxa(&$add, &$toRmv)
    {
        foreach ($toRmv as $rmv) {                                              print("\nRemove[".$rmv->getId()."] ADD: ".$add->getId());
            $this->mergeData($rmv, $add, 'Taxon');
            $this->removeDuplicate($rmv);
        }
    }
    private function deleteAndMergeDuplicateSrcs()
    {
        $this->handleHystrixDupes();
        $this->mergeBioscienceDupes();
    }
    private function handleHystrixDupes()
    {
        $add = $this->getEntity('Source', 972);
        $add->setDisplayName('Hystrix the Italian Journal of Mammalogy');
        $add->setLinkUrl('http://www.italian-journal-of-mammalogy.it/');
        $rmv = $this->getEntity('Source', 2555);                                print("\nRemove[".$rmv->getId()."] ADD: ".$add->getId());
        $rmv->setDisplayName($rmv->getDisplayName().' delete');
        $this->removeContributors($rmv);
        $this->mergeData($rmv, $add, 'Source', 'Publication');
        $this->em->remove($rmv);

        $this->handleCitationDupes();
        $this->handleLastCitationDupes();
    }
    private function handleCitationDupes()
    {
        $add = $this->getEntity('Source', 2109);
        $rmv = $this->getEntity('Source', 2556);                                print("\nRemove[".$rmv->getId()."] ADD: ".$add->getId());
        $rmv->setDisplayName($rmv->getDisplayName().' delete');
        $this->removeContributors($rmv);
        $this->mergeData($rmv, $add, 'Source', 'Citation');
        $this->em->remove($rmv);
    }
    private function handleLastCitationDupes()
    {
        $add = $this->getEntity('Source', 1694);
        $rmv = $this->getEntity('Source', 2711);                                print("\nRemove[".$rmv->getId()."] ADD: ".$add->getId());
        $rmv->setDisplayName($rmv->getDisplayName().' delete');
        $this->removeContributors($rmv);
        $this->mergeData($rmv, $add, 'Source', 'Citation');
        $this->em->remove($rmv);
    }
    private function removeContributors($src)
    {
        $contribs = $src->getContributors();
        foreach ($contribs as $contrib) {
            $src->removeContributor($contrib);
        }
    }
    private function mergeBioscienceDupes()
    {
        $add = $this->getEntity('Source', 10);
        $rmv = $this->getEntity('Source', 1262);                                print("\nRemove[".$rmv->getId()."] ADD: ".$add->getId());
        $rmv->setDisplayName($rmv->getDisplayName().' delete');
        $this->mergeData($rmv, $add, 'Source', 'Publication');
        $this->em->remove($rmv);

    }
/** ========================== REMOVE ======================================= */
    private function removeDuplicate($rmv)
    {
        $this->em->remove($rmv);
        $this->em->flush();
        $this->em->remove($rmv);
    }
/** ========================= MERGE DATA ==================================== */
    /**
     * Before removing an entity, all data can be merged/added into an entity.
     * The entity being removed is then persisted and deleted.
     * @param  [str] $rmvId       ID of the entity being removed
     * @param  [str] $addId       ID of the entity being merged into
     * @param  [str] $entityClass Entity classname
     * @param  [str] $detail      Classname of a detail entity to remove.
     */
    protected function mergeData($rmv, $add, $entityClass, $detail = false)
    {
        if ($detail) { $this->removeDetailEntityData($detail, $rmv); }
        $this->transferData($entityClass, $add, $rmv);

        $this->persistEntity($add, true);
        $this->persistEntity($rmv);
    }
    private function removeDetailEntityData($type, $coreEntity)
    {
        $getDetail = 'get'.$type;
        $removeDetail = 'remove'.$type;
        $detailEntity = $coreEntity->$getDetail();
        $coreEntity->$removeDetail();
        $this->em->remove($detailEntity);
        // $this->em->flush();
    }
/* ------------------- MERGE ENTITY DATA ------------------------------------ */
    private function transferData($entityClass, &$add, &$rmv)
    {
        $this->transferChildren($rmv, $add, $entityClass);
        $this->transferInts($rmv, $add, $entityClass);
        if ($entityClass !== 'Taxon') { return; }
        if (!$add->getParentTaxon() && $rmv->getParentTaxon()) {
            $add->setParentTaxon($rmv->getParentTaxon());
        }
    }


    /**
     * Moves the children to the new entity.
     * @param  [obj] $rmv         Entity being removed
     * @param  [obj] $add         Entity data is being merged into
     * @param  [str] $entityClass Entity classname
     */
    private function transferChildren($rmv, &$add, $type)
    {
        $map = [
            'Location' => [ 'ChildLocs', 'ParentLocation' ],
            'Source' =>   [ 'ChildSources', 'ParentSource' ],
            'Taxon' =>    [ 'ChildTaxa', 'ParentTaxon' ]
        ];
        $getFunc = 'get'.$map[$type][0];
        $setFunc = 'set'.$map[$type][1];
        $children = $rmv->$getFunc();
        if (!count($children)) { return; }                                      print("\n   CHILDREN FOUND = ".count($children));

        foreach ($children as $child) {                                         print("\n       MOVING ".$child->getId());
            $child->$setFunc($add);
            $this->persistEntity($child);
        }
    }
    /**
     * Sets the new entity into each interaction. The entity-class typically is
     * typically also the property name, except for the Subject and Object taxa.
     * @param  [obj] $rmv         Entity being removed
     * @param  [obj] $add         Entity data is being merged into
     * @param  [str] $entityClass Entity classname
     */
    private function transferInts($rmv, &$add, $entityClass)
    {
        $prop = $entityClass;

        foreach ($rmv->getInteractions() as $int) {
            if ($entityClass === 'Taxon') { $prop = $this->getRole($rmv, $add, $int); }
            $setFunc = 'set'.$prop;
            $int->$setFunc($add);
            $this->persistEntity($int);
        }
    }
    private function getRole($rmv, $add, $int)
    {
        return $int->getSubject() === $rmv ? 'Subject' : 'Object';
    }
/* ======================== down ============================================ */
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
